<?php 

use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Views\PhpRenderer;
use ChurchCRM\dto\SystemURLs;
use ChurchCRM\Utils\RedirectUtils;
use Propel\Runtime\Propel;

$system_url = SystemURLs::getRootPath();
$cspnonce = SystemURLs::getCSPNonce();
$connection = Propel::getConnection();

$container = $app->getContainer();
$phpviewer= new PhpRenderer("./templates/status", ["system_url"=>$system_url,"status_url"=>$system_url .'/v2/status/', "cspnonce"=>$cspnonce]);
$phpviewer->setLayout("layout.phtml");
$container['renderer'] = $phpviewer;
$container['connection'] = $connection;

$app->group('/status', function () {
    
    $this->get('', function(){
        header('Location:status/');
        exit;
    });
    $this->get('/', function(Request $request, Response $response, array $args) {
        
        $erros = $_SESSION['erros_status'];
        $status = $_SESSION['status_data'];

        $q = $this->connection->prepare("SELECT * FROM person_status ORDER BY id DESC");
        $q->execute();
        $lista = $q->fetchAll();

        if(isset($_GET['id']) && !empty($_GET['id'])){

            $q = $this->connection->prepare("SELECT * FROM person_status WHERE id=:id");
            $q->execute(array(":id"=>$_GET["id"]));
            $status = $q->fetch();

        } 

        $output=["error_str"=>$erros, 'lista'=>$lista, "status"=>$status];

        return $this->renderer->render($response, "main.phtml", $output);
    });

    $this->post('/add', function(Request $request, Response $response, array $args) {


        $valid_name = array(
            "description"=>gettext("Description is required"),
        );
        $erro=array();
        $data=array();
        foreach($_POST as $key=>$value){
            if(array_key_exists($key, $valid_name) && empty($_POST[$key]) ) {
                $erro[] = $valid_name[$key];
            }
            $data[$key] = $value;
        }
        
        ksort($data);
        if(count($erro)>0){
            $_SESSION['status_data'] = $_POST;
            $_SESSION['erros_status'] = implode('<br />', $erro);
            RedirectUtils::Redirect(SystemURLs::getRootPath() .'/v2/status/');
            exit;
        }

        

        if(!isset($_POST['id'])){    
            $fieldNames = implode(', ', array_keys($data));
            $fieldValues = ':' . implode(', :', array_keys($data));
            $sth = $this->connection->prepare("INSERT INTO person_status ($fieldNames) VALUES ($fieldValues)");
            foreach ($data as $key => $value) {
                $sth->bindValue(":$key", $value);
            }
        } else {
            $fieldDetails = NULL;
            foreach($data as $key=> $value) {
                $fieldDetails .= "$key=:$key,";
            }
            $fieldDetails = rtrim($fieldDetails, ',');
            $sth = $this->connection->prepare("UPDATE person_status SET $fieldDetails WHERE id=:id");
            foreach ($data as $key => $value) {
                $sth->bindValue(":$key", $value);
            }
        }
        $sth->execute();

        if($sth->rowCount()>0){
            RedirectUtils::Redirect(SystemURLs::getRootPath() .'/v2/status/');
            exit;
        } else {
            $_SESSION['erro'] = gettext("Erro on save status");
            $_SESSION['category'] = $_POST;
            RedirectUtils::Redirect(SystemURLs::getRootPath() .'/v2/status/');
            
        }
    });

    $this->get('/delete/{id}', function($request, $response, $args){

        
        if(isset($args["id"]) ){
            $id = $args["id"];
            $query_b = $this->connection->prepare("DELETE  from person_status WHERE id=:id");
            $query_b->execute(array(':id'=>$id));
            
            RedirectUtils::Redirect(SystemURLs::getRootPath() .'/v2/status/');
        } else {
            $_SESSION['erro'] = gettext("ID not found");
            RedirectUtils::Redirect(SystemURLs::getRootPath() .'/v2/status/');
        }
    });
});
