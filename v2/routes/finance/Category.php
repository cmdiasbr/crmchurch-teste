<?php 

use v2\routes\finance\Controller;
use ChurchCRM\Utils\RedirectUtils;
use ChurchCRM\dto\SystemURLs;
class CategoryFinance extends Controller{


    public function lista( $request, $response, array $args) {            
        
        $erros = $_SESSION['erros_fincance_category'];
        //$success = $_SESSION['success_fincance_category'];
        $category_finance = $_SESSION['category_finance_data'];
        $messages = $this->flash->getMessages();
        $q = $this->connection->prepare("SELECT * FROM finance_category ORDER BY id DESC");
        $q->execute();
        $_lista = $q->fetchAll();


        if($messages !=null && isset($messages['success'])){
            $success = $messages['success'][0];
        }

        if(isset($_GET['id']) && !empty($_GET['id'])){

            $q = $this->connection->prepare("SELECT * FROM finance_category WHERE id=:id");
            $q->execute(array(":id"=>$_GET["id"]));
            $category_finance = $q->fetch(PDO::FETCH_ASSOC);
            
        } 
        
        $output =["error_str"=>$erros, 'success'=>$success, 'lista'=>$_lista, "category_finance"=>$category_finance, "page_title"=>"Finance Category", "category_title"=>"Finance Category"];;
        // var_dump($output);
        // die();
        unset($_SESSION['erros_fincance_category']);
        unset($_SESSION['category_finance_data']);
        return $this->renderer->render($response, "category_tpl.phtml", $output);
    }

    public function create($req, $res){
        $valid_name = array(
            "name"=>gettext("Category name  is required"),
        );
        $erro=array();
        $data=array();
        foreach($_POST as $key=>$value){
            if(array_key_exists($key, $valid_name) && empty($_POST[$key]) ) {
                $erro[] = $valid_name[$key];
            }
            $data[$key] = $value;
        }
        
        ksort($data);
        
        if(count($erro)>0){
            $_SESSION['category_finance_data'] = $_POST;
            $_SESSION['erros_fincance_category'] = implode('<br />', $erro);
            RedirectUtils::Redirect(SystemURLs::getRootPath() .'/v2/finance/category');
            exit;
        }

        

        if(!isset($_POST['id'])){    
            $fieldNames = implode(', ', array_keys($data));
            $fieldValues = ':' . implode(', :', array_keys($data));
            $sth = $this->connection->prepare("INSERT INTO finance_category ($fieldNames) VALUES ($fieldValues)");
            foreach ($data as $key => $value) {
                $sth->bindValue(":$key", $value);
            }
        } else {
            $fieldDetails = NULL;
            foreach($data as $key=> $value) {
                $fieldDetails .= "$key=:$key,";
            }
            $fieldDetails = rtrim($fieldDetails, ',');
            $sth = $this->connection->prepare("UPDATE finance_category SET $fieldDetails WHERE id=:id");
            foreach ($data as $key => $value) {
                $sth->bindValue(":$key", $value);
            }
        }
        $sth->execute();
        //var_dump($sth, $sth->rowCount()); die();
        if($sth->rowCount()>0 || $sth){
            //$_SESSION['success_fincance_category'] = "Successfully";
            $this->flash->addMessage('success', 'Successfully');
            //RedirectUtils::Redirect(SystemURLs::getRootPath() .'/v2/finance/category');
            return $res->withStatus(302)->withHeader('Location',SystemURLs::getRootPath() . '/v2/finance/category');
            exit;
        } else {
            $_SESSION['erros_fincance_category'] = gettext("Erro on save category");
            $_SESSION['category_finance_data'] = $_POST;
            RedirectUtils::Redirect(SystemURLs::getRootPath() .'/v2/finance/category');
            
        }

    }


    public function delete($request, $response, $args){
        if(isset($args["id"]) ){
            $id = $args["id"];
            $query_b = $this->connection->prepare("DELETE  from finance_category WHERE id=:id");
            $query_b->execute(array(':id'=>$id));
            
            RedirectUtils::Redirect(SystemURLs::getRootPath() .'/v2/finance/category');
        } else {
            $_SESSION['erro'] = gettext("ID not found");
            RedirectUtils::Redirect(SystemURLs::getRootPath() .'/v2/finance/category');
        }
    }
}