<?php 
namespace v2\routes\finance;
use Psr\Container\ContainerInterface;

abstract class Controller{
    
    protected $connection;
    protected $rendered;
    protected $ci;

    /**
     * Controller constructor.
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface  $container)
    {
        $this->ci = $container;
        $this->container = $container;
        $this->renderer=$this->ci['renderer'];
        $this->flash=$this->ci['flash'];
        $this->connection = $this->ci['connection'];
        $this->params = $this->ci['params'];
        
    }

    /**
     * @param $name
     * @return mixed
     */
    public function __get($name)
    {
        if ($this->ci->has($name)) {
            return $this->ci->get($name);
        }
    }
}