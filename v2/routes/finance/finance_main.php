<?php 

use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Flash\Messages;
use Slim\Views\PhpRenderer;
use ChurchCRM\dto\SystemURLs;
use ChurchCRM\Utils\RedirectUtils;
use Propel\Runtime\Propel;

$system_url = SystemURLs::getRootPath();
$cspnonce = SystemURLs::getCSPNonce();
$connection = Propel::getConnection();

$container = $app->getContainer();

$params = [
    "finace_type"=>[
        1=>"Revenue",
        2=>"Expense"
    ],

    "ficance_status"=>[
        0=>"Inactive",
        1=>"Active"
    ],
    "system_url"=>$system_url,
    "finance_url"=>$system_url .'/v2/finance/', 
    "cspnonce"=>$cspnonce,
];


$phpviewer= new PhpRenderer("./templates/finance", $params);
$phpviewer->setLayout("layout.phtml");

$container['params'] = $params;
$container['flash'] = function () {
    return new Messages();
};

$container['renderer'] = $phpviewer;
$container['connection'] = $connection;

require_once __DIR__.'/Controller.php';
require_once __DIR__.'/Category.php';
require_once __DIR__.'/Finance.php';

$app->group('/finance', function () {
    
    $this->get('/category', \CategoryFinance::class . ':lista');
    $this->post('/category/create', \CategoryFinance::class . ':create');
    $this->get('/category/delete/{id}', \CategoryFinance::class . ':delete');
    
    $this->get('', function(){
        header('Location:finance/');
        exit;
    });


    $this->get('/', \Finance::class . ':list');
    $this->post('/save', \Finance::class . ':save');    
    $this->get('/add', \Finance::class . ':add'); 
    $this->get('/delete/{id}', \Finance::class . ':delete');
    $this->get('/report', \Finance::class . ':report');
});
