<?php 
use ChurchCRM\dto\SystemURLs;
use FPDF;
use ChurchCRM\Utils\RedirectUtils;
use v2\routes\finance\Controller;
class Finance extends Controller{



    public function list($request, $response, array $args) {
        
        $messages = $this->flash->getMessages();
        $output=[];
        $pg = isset($_GET['page']) ? $_GET['page'] : 1;
        $limit = 20;
        $offset = ($pg * $limit) - $limit;
        $cond=[];
        $where = 'WHERE 1=1';
        $output["link_page"]='';
        if(isset($_GET['term']) && !empty($_GET['term'])){
            $where .= " AND finance.description LIKE :term";
            $cond[":term"] = '%'.$_GET['term'].'%';
            $output["link_page"] = $this->finance_ul ."?term=".$_GET['term'].'&page=';
        } else {
            $output["link_page"] = $this->finance_ul.'?page=';
        }

        if($messages !=null){
            $output["success"] = isset($messages['success']) ? $messages['success'][0]:null;
        }

        $sql_main = "SELECT * FROM finance 
        INNER JOIN finance_category cat on cat.id= finance.category_id 
        $where ORDER BY finance_id";
        /**
         * I can't believe I did that
         */
        $count_sql = str_replace("*", "COUNT(*) as total ", $sql_main);

        
        $c = $this->connection->prepare($count_sql);
        $c->execute($cond);
        $res_count = $c->fetch(PDO::FETCH_ASSOC);

        $output["max_page"] = ceil(intval($res_count["total"])/$limit);
        $output["count"] = $res_count;
        $output["current_page"] = $pg;



        $q = $this->connection->prepare("$sql_main DESC LIMIT $offset, $limit ");
        $q->execute($cond);
        $list = $q->fetchAll(PDO::FETCH_ASSOC);

        $output['list']=$list;

        return $this->renderer->render($response, "list.phtml", $output);
    }

    public function save($request, $response, array $args) {

        $valid_name = array(
            "description"=>gettext("Description is required"),
            "category_id"=>gettext("Category is required"),
            "amount"=>gettext("Amount is required"),
            "date"=>gettext("Date is required"),
        );
        $redirect_param = isset($_POST['finance_id']) ? "?id=".isset($_POST['finance_id']):'';
        $erro=array();
        $data=array();
        foreach($_POST as $key=>$value){
            if(array_key_exists($key, $valid_name) && empty($_POST[$key]) ) {
                $erro[] = $valid_name[$key];
            }
            $data[$key] = $value;
        }
        
        ksort($data);
        
        if(count($erro)>0){
            $_SESSION['finance_data'] = $_POST;
            $this->flash->addMessage('erro_finance', implode('<br />', $erro));
            RedirectUtils::Redirect(SystemURLs::getRootPath() .'/v2/finance/add'.$redirect_param);
            exit;
        }
        $data["status"] = 1;
        $data["amount"] = floatval(str_replace(".","", $data["amount"]));
        if(!isset($_POST['finance_id'])){    
            
            $fieldNames = implode(', ', array_keys($data));
            $fieldValues = ':' . implode(', :', array_keys($data));
            $sth = $this->connection->prepare("INSERT INTO finance ($fieldNames) VALUES ($fieldValues)");
            foreach ($data as $key => $value) {
                $sth->bindValue(":$key", $value);
            }
        } else {
            
            $fieldDetails = NULL;
            foreach($data as $key=> $value) {
                $fieldDetails .= "$key=:$key,";
            }
            $fieldDetails = rtrim($fieldDetails, ',');
            $sth = $this->connection->prepare("UPDATE finance SET $fieldDetails WHERE finance_id=:finance_id");
            foreach ($data as $key => $value) {
                $sth->bindValue(":$key", $value);
            }
        }
        $sth->execute();

        if($sth->rowCount()>0){
            $this->flash->addMessage('success', gettext("Successfully"));
            RedirectUtils::Redirect(SystemURLs::getRootPath() .'/v2/finance/');
            exit;
        } else {
            
            $this->flash->addMessage('erro_finance', gettext("Erro on save finance"));
            $_SESSION['finance_data'] = $_POST;
            
            RedirectUtils::Redirect(SystemURLs::getRootPath() .'/v2/finance/add'. $redirect_param);
        }
    }


    public function add($req, $res, $args){

        $output["page_title"]="Add Finance";
        
        $messages = $this->flash->getMessages();
        $finance = $_SESSION['finance_data'];


        if($messages !=null){
            $output["error_str"] = isset($messages['erro_finance']) ? $messages['erro_finance'][0]:null;
            $output["success"] = isset($messages['success']) ? $messages['success'][0]:null;
        }

        $cat= $this->connection->prepare("SELECT * FROM finance_category");
        $cat->execute();
        $output["categories"] =  $cat->fetchAll(PDO::FETCH_ASSOC);

        if(isset($_GET['id']) && !empty($_GET['id'])){
            $output["page_title"]="Edit Finance";
            $q = $this->connection->prepare("SELECT * FROM finance WHERE finance_id=:id");
            $q->execute(array(":id"=>$_GET["id"]));
            $output["finance"] = $q->fetch(PDO::FETCH_ASSOC);

        } 

        
        return $this->renderer->render($res, "form_finance.phtml", $output);
    }


    public function report($request, $response){
        $data = $request->getParams();
        $output["page_title"]="Finance Report";
        $output["result"] = null;
        $output["filtered"] = false;
        $output["self_link"]='';
        $field_valid = ["date_ini", "date_end", "date_end", "category_id", "description"];
        $cout=0;
        foreach($data as $key=>$v){
            if(!in_array($key, $field_valid)){
                unset($data[$key]);
            }
            if($cout==0){
                $output["self_link"] .='?'.$key.'='.$v;
            } else {
                $output["self_link"] .='&'.$key.'='.$v;
            }
            $cout++;
        }
        $categories = $this->connection->prepare("SELECT * FROM finance_category ORDER BY id DESC");
        $categories->execute();
        $output["categories"] = $categories->fetchAll();

        if(count($data)>0){
            $output["filtered"] = true;
            $code = 200;
            $payload = [];
            $cond=[];
            $where ="1=1"; 
            $join='';
            $select="f.*";
            if(isset($data["date_ini"]) && !empty($data["date_ini"])){
                $data_end = isset($data["date_end"]) && !empty($data["date_end"]) ? $data["date_end"]:date('Y-m-d');
                $where .=" AND f.date BETWEEN :date_ini AND :date_end";
                $cond[":date_ini"]=$data["date_ini"];
                $cond[":date_end"]=$data_end;
            }

            if(isset($data["type"]) && !empty($data["date_end"]) ){
                $where .=" AND f.type =:type";
                $cond[":type"]=$data["type"];
            }

            if(isset($data["category_id"]) && !empty($data["category_id"]) ){
                $where .=" AND f.category_id =:category_id";
                $cond[":category_id"]=$data["category_id"];
            }

            if(isset($data['description']) && !empty($data['description'])){
                $where .= " AND finance.description LIKE :term";
                $cond[":description"] = '%'.$data['description'].'%';
            }
            
            $report = $this->connection->prepare("SELECT f.*, fc.name as category_name, fc.id FROM finance f 
            INNER JOIN finance_category fc ON fc.id=f.category_id WHERE $where ORDER BY date ASC");
            $report->execute($cond);
            $result = $report->fetchAll(PDO::FETCH_ASSOC);
            
            $output["result"] = $result;
       
        }

        if($output["filtered"] && isset($_GET['pdf']) ){
        
            $pdf= new FPDF("L","pt","A4");
    
            $pdf->AddPage();
            
            $pdf->SetFont('arial','B',18);
            $pdf->Cell(0,5,iconv('UTF-8', 'ISO-8859-1', _("Finance Report")),0,1,'C');
            $pdf->Cell(0,5,"","B",1,'C');
            $pdf->Ln(50);
            
            //cabeçalho da tabela 
            $pdf->SetFont('arial','B',14);
            $pdf->SetFillColor(193,229,252); // Background color of header 
            $pdf->Cell(225,20,iconv('UTF-8', 'ISO-8859-1',_("Description")),1,0,"L");
            $pdf->Cell(140,20,_("Amount"),1,0,"L");
            $pdf->Cell(130,20,_("Category"),1,0,"L");
            $pdf->Cell(130,20,_("Date"),1,0,"L");
            $pdf->Cell(160,20,_("Type"),1,1,"L");
            
            //linhas da tabela
            $pdf->SetFont('arial','',12);
            foreach($output["result"] as $result){
                $result = array_map(function($str){
                    return iconv('UTF-8', 'ISO-8859-1', $str);
                }, $result);
    
                $pdf->Cell(225,20,$result["description"],1,0,"L");
                $pdf->Cell(140,20,$result["amount"],1,0,"L");
                $pdf->Cell(130,20,$result["category_name"],1,0,"L");
                $pdf->Cell(130,20,$result["date"],1,0,"L");
                $pdf->Cell(160,20,$this->params["finace_type"][$result["type"]],1,1,"L");
            }
            
            $pdf->Output("file-report-".date('Y-m-d').".pdf", "D");
            exit();
            
        }

        return $this->renderer->render($response, "report_main.phtml", $output);
        // if($result){
        //     $payload = json_encode($result);
        // } else {
        //     $code = 500;
        //     $payload=json_encode(["code"=>500, "message"=>gettext("Not Found")]);
        // }
        
        // $response->getBody()->write($payload);
        // return $response
        //         ->withHeader('Content-Type', 'application/json')
        //         ->withStatus($code);
    }


    public function delete($request, $response, $args){

        
        if(isset($args["id"]) ){
            $id = $args["id"];
            $query_b = $this->connection->prepare("DELETE  from finance WHERE finance_id=:id");
            $query_b->execute(array(':id'=>$id));
            
            RedirectUtils::Redirect(SystemURLs::getRootPath() .'/v2/finance/');
        } else {
            $_SESSION['erro'] = gettext("ID not found");
            RedirectUtils::Redirect(SystemURLs::getRootPath() .'/v2/finance/');
        }
    }

}