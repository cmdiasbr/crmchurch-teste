<?php 
use Slim\Http\Request;
use Slim\Http\Response;
use Propel\Runtime\Propel;
use ChurchCRM\dto\SystemURLs;
$connection = Propel::getConnection();

$container = $app->getContainer();
$container['renderer'] = $phpviewer;
$container['connection'] = $connection;
$container["root_url"] = SystemURLs::getRootPath();
$app->group('/notes', function () {
    
    $this->get('', function(){
        header('Location:/');
        exit;
    });
    $this->get('/find/', function(Request $request, Response $response, array $args) {

        
        $title = $request->getParam('term') !=null ? $request->getParam('term') :null;
        $code = 200;
        $payload=[];
        if($title!=null){
            $q = $this->connection->prepare("SELECT note_nte.*, p.per_LastName AS author FROM note_nte
             INNER JOIN person_per p on p.per_ID = note_nte.nte_EnteredBy   
             WHERE nte_Title LIKE :title OR nte_Text LIKE :texto ORDER BY nte_ID DESC");
            $q->execute(array(
                ":title"=>'%'.$title.'%',
                ":texto"=>'%'.$title.'%',
            ));
            $lista = $q->fetchAll(PDO::FETCH_OBJ);
            $respons=[];
            if($lista){
                foreach ($lista as $value) {
                    $url_edit = $this->root_url.'/NoteEditor.php?NoteID='.$value->nte_ID.'&';
                    
                    if ($value->nte_per_ID != '') {
                        $url_edit = $url_edit.'PersonID='.$value->nte_per_ID;
                    } else {
                        $url_edit = $url_edit.'FamilyID='.$value->nte_per_ID;
                    }
                    
                    $delete_link = $this->root_url.'/NoteDelete.php?NoteID='.$value->nte_per_ID;
                    
                    

                    $respons[] = array(
                        "datetime"=>$value->nte_DateEntered,
                        "deleteLink"=>$delete_link,
                        "editLink"=>$url_edit,
                        "header"=>gettext("by"). ' '.$value->author,
                        "headerLink"=>"",
                        "id"=>21,
                        "key"=>$value->nte_DateEntered.'-'.$value->nte_ID,
                        "slim"=>false,
                        "style"=>"fa-sticky-note bg-green",
                        "text"=>$value->nte_Text,
                        "title"=>$value->nte_Title,
                        "type"=>$value->nte_Type,
                        "year"=>date('Y', strtotime($value->nte_DateEntered))
                    );
                }
            }
            $payload = json_encode($respons);

        } else {
            $code = 500;
            $payload = json_encode( array('code'=>$code, 'message'=>gettext("Not found")) );
        }

        $response->getBody()->write($payload);
        return $response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus($code);
    });

    
});