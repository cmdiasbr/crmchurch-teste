<?php 

error_reporting(E_ALL);
ini_set('display_errors', '1');
define("DS",DIRECTORY_SEPARATOR);
define('APP_DIR',  dirname(__FILE__) .DS.'app');
define('ROOT_DIR',  dirname(__FILE__));
require '../Include/Config.php';
require __DIR__ . '/core/core.php';
require_once dirname(__FILE__).'/../vendor/autoload.php';
require __DIR__ . "/app/route.php";
