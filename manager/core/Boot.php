<?php 

class Boot
{
    public static function route($class_method) {
        list($class, $method) = explode('@', $class_method);
        return function() use ($class, $method) {
            return call_user_func_array(array(new $class, $method), func_get_args());
        };
    }
}
