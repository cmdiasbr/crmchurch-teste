<?php 

require_once APP_DIR . DS . 'config/functions.php';


function _loadmodel($class){
	if(file_exists(APP_DIR .DS.'models'. DS . $class . '.php')){
		require_once APP_DIR .DS.'models'. DS . $class . '.php';
	}
}
function _load($class){
	if(file_exists(ROOT_DIR .DS.'core'. DS . $class . '.php')){
		require_once ROOT_DIR .DS.'core'. DS . $class . '.php';
	}
}
function Controllers($class){ 
    if(file_exists(APP_DIR .DS.'controllers'. DS . $class . '.php')){
        require_once APP_DIR .DS.'controllers'. DS . $class . '.php';
    } 
}


spl_autoload_register('_loadmodel');
spl_autoload_register('_load');
spl_autoload_register('Controllers');