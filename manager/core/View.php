<?php
require_once ROOT_DIR . "/libs/SimpleTemplateEngine/loader.php";
class View
{
    public $env;
    public function __construct(){
        $this->env=new SimpleTemplateEngine\Environment(APP_DIR . '/views', '.phtml');
    }

    public function render($template, $vars=[], $echo=true){
        
        if($echo) 
            echo  $this->env->render($template, $vars);
        else 
            return $this->env->render($template, $vars);
    }
}
