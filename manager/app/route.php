<?php 

use Slim\Container;
use Slim\App;


// Instantiate the app
$settings = [
    'settings' => [
        'displayErrorDetails' => true,
    ]
];

$container = new Container($settings);

$app = new App($container);
global $app;
$app->get("/", Boot::route('PainelController@index'));
$app->get("/list", Boot::route('PackagesController@list'));
$app->get("/add", Boot::route('PackagesController@add'));
$app->post("/save", Boot::route('PackagesController@save'));
$app->get("/status/{id}/{status}", Boot::route('PackagesController@status'));
$app->get("/login", Boot::route('LoginController@index'));
$app->post("/login/run", Boot::route('LoginController@run'));

$app->run();
