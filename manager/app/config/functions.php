<?php 

function url($url=false){

	if (isset($_SERVER['HTTP_HOST'])) {
		$http = isset($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) !== 'off' ? 'https' : 'http';
		$hostname = $_SERVER['HTTP_HOST'];
		$dir =  str_replace(basename($_SERVER['SCRIPT_NAME']), '', $_SERVER['SCRIPT_NAME']);

		$core = preg_split('@/@', str_replace($_SERVER['DOCUMENT_ROOT'], '', realpath(dirname(__FILE__))), NULL, PREG_SPLIT_NO_EMPTY);
		$core = $core[0];

		$tmplt = "%s://%s%s";
		$end = $dir;
		$base_url = sprintf( $tmplt, $http, $hostname, $end );
	} else {
		$base_url = 'http://localhost/';
	}    

	$base_url = $url ? $base_url . $url : $base_url;
	return $base_url; 
}

function base_directory(){
	$base = str_replace(basename($_SERVER['SCRIPT_NAME']), '', $_SERVER['SCRIPT_NAME']);
	return $base;
}

function assets($folder=null){
  $folder =  !is_null($folder) ? $folder : '';
  return base_directory() .'assets/'.$folder;
}

function redirect($to){
    header('Location:'.$to);
}