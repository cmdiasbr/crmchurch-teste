<?php

class PackagesController extends Auth{


    public function list(){
        $output["success"]=null;
        $messages = $this->flash->getMessages();
        $output=[];
        $pg = isset($_GET['page']) ? $_GET['page'] : 1;
        $limit = 20;
        $offset = ($pg * $limit) - $limit;
        
        if($messages !=null){
            $output["success"] = isset($messages['success']) ? $messages['success'][0]:null;
        }

        $sql_main = "SELECT * FROM master_packages $where ORDER BY package_id";
        /**
         * I can't believe I did that
         */
        $count_sql = str_replace("*", "COUNT(*) as total ", $sql_main);

        
        $c = $this->db->prepare($count_sql);
        $c->execute($cond);
        $res_count = $c->fetch(PDO::FETCH_ASSOC);

        $output["max_page"] = ceil(intval($res_count["total"])/$limit);
        $output["count"] = $res_count;
        $output["current_page"] = $pg;

        $q = $this->db->prepare("$sql_main DESC LIMIT $offset, $limit ");
        $q->execute($cond);
        $output["list"] = $q->fetchAll(PDO::FETCH_ASSOC);

        $this->view->render("painel.list", $output);
    }


    public function add(){

        $output["page"] = _("Add");
        $output["package"] = Session::get('package_data');


        if(isset($_GET['id']) && !empty($_GET['id'])){
            $output["page"]="Edit";
            $q = $this->db->prepare("SELECT * FROM master_packages WHERE package_id=:id");
            $q->execute(array(":id"=>$_GET["id"]));
            $output["package"] = $q->fetch(PDO::FETCH_ASSOC);

        } 

        $this->view->render("painel.form", $output);
    }


    public function save($request, $response) {
        $data = $request->getParsedBody();
        $valid_name = array(
            "description"=>gettext("Description is required"),
            "price"=>gettext("Price is required"),
        );
        $redirect_param = isset($data['package_id']) ? "?id=".isset($data['package_id']):'';
        $erro=array();
        foreach($data as $key=>$value){
            if(array_key_exists($key, $valid_name) && empty($data[$key]) ) {
                $erro[] = $valid_name[$key];
            }
        }
        
        ksort($data);
        
        if(count($erro)>0){
            
            Session::set('package_data', $data);
            $this->flash->addMessage('erro_package', implode('<br />', $erro));
            redirect(url("list").$redirect_param );
            exit;
        }
        
        $data["price"] = floatval(str_replace(".","", $data["price"]));
        if(!isset($data['package_id'])){    
            $data["created_at"] = date("Y-m-d H:i:s");
            $data["updated_at"] = date("Y-m-d H:i:s");
            $fieldNames = implode(', ', array_keys($data));
            $fieldValues = ':' . implode(', :', array_keys($data));
            $sth = $this->db->prepare("INSERT INTO master_packages ($fieldNames) VALUES ($fieldValues)");
            foreach ($data as $key => $value) {
                $sth->bindValue(":$key", $value);
            }
        } else {
            $data["updated_at"] = date("Y-m-d H:i:s");
            $fieldDetails = NULL;
            foreach($data as $key=> $value) {
                $fieldDetails .= "$key=:$key,";
            }
            $fieldDetails = rtrim($fieldDetails, ',');
            $sth = $this->db->prepare("UPDATE master_packages SET $fieldDetails WHERE package_id=:package_id");
            foreach ($data as $key => $value) {
                $sth->bindValue(":$key", $value);
            }
        }
        $sth->execute();

        if($sth->rowCount()>0){
            $this->flash->addMessage('success', _("Successfully"));
            redirect(url("list"));
            exit;
        } else {
            
            $this->flash->addMessage('erro_package', _("Erro on save package"));
            Session::set('package_data', $data);
            redirect(url("add"));
            exit;
        }
    }

    public function status($request, $response, $args){
        
        if(isset($args["id"]) && isset($args["status"]) ){
            $id = $args["id"];
            $status = $args["status"];
            $query_b = $this->db->prepare("UPDATE master_packages SET status=$status WHERE package_id=:id");
            $query_b->execute(array(':id'=>$id));
            $this->flash->addMessage('success', _("register updated successfully"));
            redirect(url("list"));
            exit;
        } else {
            $this->flash->addMessage('success', _("ID not found"));
            redirect(url("list"));
            exit;
        }
    }

}