<?php

class LoginController extends Auth{
    protected $check_login=false;
    public function index(){

        $messages = $this->flash->getMessages();
        $output["title"]="Login";
        $output["erro_login"]=null;
        if($messages !=null){
            $output["erro_login"] = isset($messages['erro_login']) ? $messages['erro_login'][0]:null;
        }

        $this->view->render("login.painel", $output);
    }


    public function run($request, $response){
        $data = $request->getParsedBody();

        $valid_name = [
            "username"=>_("Username is required"),
            "password"=>_("Password is required"),
        ];
        $erro=[];

        if(count($data)<1){
            $erro[] = _("Username and password is required");
        }

        foreach($data as $key=>$value){
            if(array_key_exists($key, $valid_name) && empty($data[$key]) ) {
                $erro[] = $valid_name[$key];
            }
        }

        if(count($erro)>0){
            $this->flash->addMessage('erro_login', implode('<br />', $erro));
            
            redirect(url('login'));
            exit;
        }

        $user_query = $this->db->prepare("SELECT * FROM master_user WHERE (username=:username OR email=:email) AND password=:password ");
        $user_query->execute([
            ":username"=>$data["username"],
            ":email"=>$data["username"],
            ":password"=>Hash::create('sha256',$data["password"], Hash::CRYPTO)
        ]);
        $user = $user_query->fetch(PDO::FETCH_ASSOC);
        if($user){
            Session::set("usuario", $user);
            redirect(url());
            exit;
        } else {
            $this->flash->addMessage('erro_login', _('Invalid login or password'));
            redirect(url('login'));
            exit;
        }
    }
}