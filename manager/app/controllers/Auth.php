<?php 

class Auth extends Controller{
    protected $check_login=true;
    protected $usuario = null;

    public function __construct()
    {
        parent::__construct();
        @session_start();
        $this->usuario = Session::get('usuario');
        $this->view->env->user = (object)$this->usuario;
        $this->view->env->title = _("Manager Packages");
        if($this->check_login)
            $this->checkAuth();
    }


    public function checkAuth(){
        
        if($this->usuario==null){
            redirect(url('login'));
            exit;
        }
    }
}