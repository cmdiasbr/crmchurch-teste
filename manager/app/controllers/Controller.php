<?php

use Slim\Views\PhpRenderer;
use Propel\Runtime\Propel;
use Slim\Flash\Messages;

abstract class Controller {
    public $app;
    public $container;
    public function __construct() {
        global $app;
        $this->app = $app;
        $this->container = $app->getContainer();
        $params = [
            "finance_url"=>$system_url .'/manager/', 
        ];
        $this->view = new View();
        $this->container['connection'] = Propel::getConnection();
        $this->db = $this->container['connection'];
        $this->container['params'] = $params;
        $this->flash= new Messages();
        

    }

    protected function _json($json=array(), $return=false, $exit=1) {
        if (!$return && !headers_sent()) {
            header("Content-Type: application/json; charset=utf-8");
            echo json_encode($json);
            $exit && exit(); //enhance Slim performance
        } else {
            return json_encode($json);
        }
    }
    protected function _params($name) {
        return $this->app->request->params($name);
    }
}