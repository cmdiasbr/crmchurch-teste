<?php
namespace classes;
use ChurchCRM\Utils\RedirectUtils;
use ChurchCRM\dto\SystemURLs;
use Propel\Runtime\Propel;

class BusinessCategory {

    public function __construct(){
        $this->system_url = SystemURLs::getRootPath();
        $this->cspnonce = SystemURLs::getCSPNonce();
        $this->connection = Propel::getConnection();
    }

    public function ListCategory(){
        $this->category=$_SESSION['category'];
        $this->chu_id=$_SESSION['user']->getChuId();
        $sSql = "SELECT * FROM business_category WHERE bus_chu_id=".$this->chu_id." ORDER BY category_id DESC";
        $q = $this->connection->prepare($sSql);
        $q->execute();
        $this->lista = $q->fetchAll();

        if(isset($_GET['id']) && !empty($_GET['id'])){

            $q = $this->connection->prepare("SELECT * FROM business_category WHERE category_id=:id AND bus_chu_id=".$this->chu_id);
            $q->execute(array(":id"=>$_GET["id"]));
            $this->category = $q->fetch();

        }


        include VIEW .'/business/business-category.phtml';
        unset($_SESSION['category']);
        unset($_SESSION['erro']);
    }

    public function save(){

        if(isset($_POST)){

            $valid_name = array(
                "name"=>gettext("Name is required"),
            );
            $erro=array();
            $data=array();
            foreach($_POST as $key=>$value){
                if(array_key_exists($key, $valid_name) && empty($_POST[$key]) ) {
                    $erro[] = $valid_name[$key];
                }
                $data[$key] = $value;
            }

            ksort($data);
            if(count($erro)>0){
                $_SESSION['category'] = $_POST;
                $_SESSION['erro'] = implode('<br />', $erro);
                RedirectUtils::Redirect('BusinessCategory.php');
                exit;
            }
            $this->chu_id=$_SESSION['user']->getChuId();
            if(!isset($_POST['category_id'])){
                $fieldNames = implode(', ', array_keys($data));
                $fieldValues = ':' . implode(', :', array_keys($data));

                $sth = $this->connection->prepare("INSERT INTO business_category ($fieldNames, bus_chu_id) VALUES ($fieldValues, $this->chu_id)");
                foreach ($data as $key => $value) {
                    $sth->bindValue(":$key", $value);
                }
            } else {
                $fieldDetails = NULL;
                foreach($data as $key=> $value) {
                    $fieldDetails .= "$key=:$key,";
                }
                $fieldDetails = rtrim($fieldDetails, ',');
                $sth = $this->connection->prepare("UPDATE business_category SET $fieldDetails WHERE category_id=:category_id AND bus_chu_id=".$this->chu_id);
                foreach ($data as $key => $value) {
                    $sth->bindValue(":$key", $value);
                }
            }
            $sth->execute();

            if($sth->rowCount()>0){
                RedirectUtils::Redirect('BusinessCategory.php');
                exit;
            } else {
                $_SESSION['erro'] = gettext("Erro on save business");
                $_SESSION['category'] = $_POST;
                RedirectUtils::Redirect('BusinessCategory.php?id='.$_POST['category_id']);
            }
        }

    }

    public function delete(){
        if(isset($_GET['category_id']) ){
            $this->chu_id=$_SESSION['user']->getChuId();
            $sSql = "DELETE from business_category WHERE category_id=:id AND bus_chu_id=".$this->chu_id;
            $query_b = $this->connection->prepare($sSql);
            $query_b->execute(array(':id'=>$_GET['category_id']));

            RedirectUtils::Redirect('BusinessCategory.php');
        } else {
            $_SESSION['erro'] = gettext("ID not found");
            RedirectUtils::Redirect('BusinessCategory.php');
        }
    }
}
