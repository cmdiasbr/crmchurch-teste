<?php 
namespace classes;
use ChurchCRM\Utils\RedirectUtils;
use ChurchCRM\dto\SystemURLs;
use Propel\Runtime\Propel;

class Business {

    public function __construct(){
        $this->system_url = SystemURLs::getRootPath();
        $this->cspnonce = SystemURLs::getCSPNonce();
        $this->connection = Propel::getConnection();
    }

    public function getBusiness(){
        $this->error_str=null;
        $this->person = null;
        $this->person_business=null;


        $q = $this->connection->prepare("SELECT * FROM business_category ORDER BY category_id DESC");
        $q->execute();
        $this->categories = $q->fetchAll();

        if(isset($_GET['PersonID']) && !empty($_GET['PersonID'])){
            $this->person_business=$_SESSION['person_business'];
            $this->error_str=$_SESSION['erro'];
            $query = $this->connection->prepare("SELECT * FROM person_per WHERE per_ID=:id");
            $query->execute(array(':id'=>$_GET['PersonID']));
            $this->person = $query->fetch(\PDO::FETCH_ASSOC);
            
            if($this->person['per_ID'] !=null){
                $query_b = $this->connection->prepare("SELECT * FROM person_business WHERE person_id=:id");
                $query_b->execute(array(':id'=>$_GET['PersonID']));
                $this->person_business = $query_b->fetch(\PDO::FETCH_ASSOC);
            }
            
        }
        
        include VIEW .'/business/form.phtml';
        unset($_SESSION['person_business']);
        unset($_SESSION['erro']);

    }

    public function save(){
        
        if(isset($_POST['person_id']) && !empty($_POST['person_id'])){        
            if(isset($_FILES['logo'])){
                /*
                * Cria o diretorio uploads
                */
                
                if(!is_dir(UPLOAD)) {
                    $diretorio = mkdir(UPLOAD , 0777);
                }
                $diretorio=UPLOAD;
                $extensao = strtolower(substr($_FILES['logo']['name'], -4)); //pega a extensao do arquivo
                $novo_nome = md5(time()) . $extensao; //define o nome do arquivo
                move_uploaded_file($_FILES['logo']['tmp_name'], $diretorio .DS.$novo_nome); //efetua o upload
                $_POST['logo'] = 'images/Business/'.$novo_nome;
            }    


            $valid_name = array(
                "phone"=>gettext("Phone is required"), 
                "name"=>gettext("Name is required"),
                "description"=>gettext("Description is required"),
            );
            $erro=array();
            $data=array();
            foreach($_POST as $key=>$value){
                if(array_key_exists($key, $valid_name) && empty($_POST[$key]) ) {
                    $erro[] = $valid_name[$key];
                }
                $data[$key] = $value;
            }
            
            ksort($data);
            if(count($erro)>0){
                $_SESSION['person_business'] = $_POST;
                $_SESSION['erro'] = implode('<br />', $erro);
                RedirectUtils::Redirect('PersonNewBusiness.php?PersonID='.$_POST['person_id']);
                exit;
            }
            if(!isset($_POST['id'])){    
                $fieldNames = implode(', ', array_keys($data));
                $fieldValues = ':' . implode(', :', array_keys($data));
                $sth = $this->connection->prepare("INSERT INTO person_business ($fieldNames) VALUES ($fieldValues)");
                foreach ($data as $key => $value) {
                    $sth->bindValue(":$key", $value);
                }
            } else {
                $fieldDetails = NULL;
                foreach($data as $key=> $value) {
                    $fieldDetails .= "$key=:$key,";
                }
                $fieldDetails = rtrim($fieldDetails, ',');
                $sth = $this->connection->prepare("UPDATE person_business SET $fieldDetails WHERE id=:id");
                foreach ($data as $key => $value) {
                    $sth->bindValue(":$key", $value);
                }
            }
            $sth->execute();

            if($sth->rowCount()>0){
                RedirectUtils::Redirect('PersonView.php?PersonID='.$_POST['person_id']);
                exit;
            } else {
                $_SESSION['erro'] = gettext("Erro on save business");
                $_SESSION['person_business'] = $_POST;
                RedirectUtils::Redirect('PersonNewBusiness.php?PersonID='.$_POST['person_id']);
            }
        }
        
    }

    public function delete(){

        
        if(isset($_GET['PersonID']) ){
            $query_b = $this->connection->prepare("SELECT * FROM person_business WHERE person_id=:id");
            $query_b->execute(array(':id'=>$_GET['PersonID']));
            $this->person_business = $query_b->fetch(\PDO::FETCH_ASSOC);    

            if(isset($this->person_business['person_id'])){
                $sth = $this->connection->prepare("UPDATE person_business SET logo=NULL WHERE id=:id");
                $sth->execute(array(":id"=> $this->person_business["id"]));
                unlink(UPLOAD .DS.str_replace("images/Business/", "",$this->person_business['logo']));
            }
            RedirectUtils::Redirect('PersonNewBusiness.php?PersonID='.$this->person_business['person_id']);
        } else {
            $_SESSION['erro'] = gettext("ID not found");
            RedirectUtils::Redirect('PersonNewBusiness.php?PersonID='.$_POST['person_id']);
        }
    }
}
