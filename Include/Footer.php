<?php
/*******************************************************************************
 *
 *  filename    : Include/Footer.php
 *  last change : 2002-04-22
 *  description : footer that appear on the bottom of all pages
 *
 *  http://www.churchcrm.io/
 *  Copyright 2001-2002 Phillip Hullquist, Deane Barker, Philippe Logel
  *
 ******************************************************************************/

use ChurchCRM\dto\SystemURLs;
use ChurchCRM\Service\SystemService;
use ChurchCRM\Bootstrapper;

$isAdmin = $_SESSION['user']->isAdmin();
?>
</section><!-- /.content -->

</div>
<!-- /.content-wrapper -->
<footer class="main-footer">
    <div class="pull-right">
        <b><?//= gettext('Version') ?></b> <?//= $_SESSION['sSoftwareInstalledVersion'] ?>
    </div>
    <strong><?= gettext('Copyright') ?> &copy; <?= SystemService::getCopyrightDate() ?> <a href="#" target="_blank"><b></b>WebHolding</a>.</strong> <?= gettext('All rights reserved') ?>.
    
</footer>

<!-- The Right Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
    <div class="tab-content">
        <div class="tab-pane active" id="control-sidebar-tasks-tab">
            <?= gettext('You have') ?> &nbsp; <span class="label label-danger"><?= $taskSize ?></span>
            &nbsp; <?= gettext('task(s)') ?>
            <br/><br/>
            <ul class="control-sidebar-menu">
                <?php foreach ($tasks as $task) {
    $taskIcon = 'fa-info bg-green';
    if ($task['admin']) {
        $taskIcon = 'fa-lock bg-yellow-gradient';
    } ?>
                    <!-- Task item -->
                    <li>
                        <a href="<?= $task['link'] ?>">
                            <i class="menu-icon fa fa-fw <?= $taskIcon ?>"></i>
                            <div class="menu-info">
                                <h4 class="control-sidebar-subheading"
                                    title="<?= $task['desc'] ?>"><?= $task['title'] ?></h4>
                            </div>
                        </a>

                    </li>
                    <!-- end task item -->
                    <?php
} ?>
            </ul>
            <!-- /.control-sidebar-menu -->

        </div>
        <!-- /.tab-pane -->
    </div>
</aside>
<!-- The sidebar's background -->
<!-- This div must placed right after the sidebar for it to work-->
<div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
</div><!-- ./wrapper -->

<!-- Bootstrap 3.3.5 -->


<script src="skin/external/bootstrap/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="skin/external/adminlte/adminlte.min.js"></script>

<!-- InputMask -->
<script src="skin/external/inputmask/jquery.inputmask.bundle.min.js"></script>
<script src="skin/external/inputmask/inputmask.date.extensions.min.js"></script>
<script src="skin/external/inputmask/inputmask.extensions.min.js"></script>

<script src="skin/external/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<script src="skin/external/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="skin/external/bootstrap-timepicker/bootstrap-timepicker.min.js"></script>

<script src="skin/external/datatables/pdfmake.min.js"></script>
<script src="skin/external/datatables/vfs_fonts.js"></script>
<script src="skin/external/datatables/datatables.min.js"></script>

<!-- <script src="/skin/external/chartjs/Chart.min.js"></script> -->
<script src="skin/external/select2/select2.min.js"></script>

<script src="skin/external/bootstrap-notify/bootstrap-notify.min.js"></script>
<script src="skin/external/fullcalendar/fullcalendar.min.js"></script>
<script src="skin/external/bootbox/bootbox.min.js"></script>
<script src="skin/external/fastclick/fastclick.js"></script>
<script src="skin/external/bootstrap-toggle/bootstrap-toggle.js"></script>
<script src="skin/external/i18next/i18next.min.js"></script>
<script src="locale/js/<?= Bootstrapper::GetCurrentLocale()->getLocale() ?>.js"></script>
<script src="skin/external/bootstrap-validator/validator.min.js"></script>

<script src="skin/js/IssueReporter.js"></script>
<script src="skin/js/DataTables.js"></script>
<script src="skin/js/Tooltips.js"></script>
<script src="skin/js/Events.js"></script>
<script src="skin/js/Footer.js"></script>

<!-- App js -->
        <script src="skin/js/app.js"></script>

<?php if (isset($sGlobalMessage)) {
        ?>
    <script nonce="<?= SystemURLs::getCSPNonce() ?>">
        $("document").ready(function () {
            showGlobalMessage("<?= $sGlobalMessage ?>", "<?=$sGlobalMessageClass?>");
        });
    </script>
    <?php
    } ?>

<?php  include_once('analyticstracking.php'); ?>
</body>
</html>
<?php

// Turn OFF output buffering
ob_end_flush();

// Reset the Global Message
$_SESSION['sGlobalMessage'] = '';

?>
