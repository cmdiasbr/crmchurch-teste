jQuery(document).ready(function($){
    Vue.config.devtools=true;
    var ModalVue = {
        template:"#tpl-modal",
    }
    var AppMain = {
        template:"#main-app",
        data:function(){
            return{
                from:null,
                item:null,
                opened:false,
                data_note:null,
                search_note:'',
                items:[],
                loading:false,
                is_search:false,
            }
        },
        components:{
            "modal-notas":ModalVue
        },
        methods:{
            openModal:function(from, data){
                this.item = data;
                this.from = from;
                var el_modal= $('#view-nota');
                this.search_note = '';
                if(from=='item'){
                    this.is_search = false
                }
                el_modal.modal('show');
                this.opened = true;
                var self = this;
                el_modal.on('hidden.bs.modal', function (e) {
                    self.opened = false;
                    self.item = null;
                    self.from = null;
                    self.search_note = '';
                    self.items = []
                });
            },
            SearchNote:function(){
                var self = this;
                self.loading=true;
                self.is_search=true;
                $.ajax({
                    url:window.CRM.root + '/v2/notes/find/',
                    data:{term:self.search_note},
                    success:function(res){
                        console.log(self.is_search)
                        self.items = res;
                        self.loading=false
                    },
                    error:function(){
                        self.loading=false
                    }
                });
            }
        },
        created:function(){
            this.data_note = data_note
            this.title_search = window.title_search;
        }

    }
    var AppNote = new Vue({
        el:"#notes-app",
        template:'<app-main-notas />',
        components:{
            "app-main-notas":AppMain
        },
    });
})
/*

var item=null
$('.open-nota').click(function(){
    item = $(this).data("item");
    contentModalNote(item)
    
});

function contentModalNote(dados){
    var fullContent = $("#full-content");
    var title = $("#view-Note")
    var el_modal= $('#view-nota');
    var modal_content = $("#content-note").html("");
    var content_search_modal = $("#content-search-modal");
    var html = '';
    var text_title = '';

    el_modal.modal('show');
    el_modal.on('hidden.bs.modal', function (e) {
        content_search_modal.css({"display":"none"});
        fullContent.removeAttr('style');
        modal_content.html('');
        title.text("")
        dados  =null;
        item=null;
        html = '';
        text_title = '';
    });
    
    el_modal.on('shown.bs.modal', function (e) {
        if(dados !=null && typeof dados !='undefined'){
            content_search_modal.css({"display":"none"});
            html +=dados.text.replace(/&nbsp;/g, ' ');
            text_title = dados.title;
            fullContent.css({'max-width':'95%'});
            title.text(text_title);
            modal_content.html(html);
            
        } else {
            title.text(window.title_search)
            content_search_modal.css({"display":"block"});
        }
    });
    
}
*/