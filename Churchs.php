<?php
/*******************************************************************************
 *
 *  filename    : SystemSettings.php
 *  description : setup de systema settings
 *
 *  http://www.churchcrm.io/
 *  Copyright 2001-2002 Phillip Hullquist, Deane Barker
 *
 ******************************************************************************/

// Include the function library
require 'Include/Config.php';
require 'Include/Functions.php';

use ChurchCRM\dto\LocaleInfo;
use ChurchCRM\dto\SystemConfig;
use ChurchCRM\Utils\InputUtils;
use ChurchCRM\dto\SystemURLs;
use ChurchCRM\Utils\RedirectUtils;
use ChurchCRM\Bootstrapper;

// Security
if (!$_SESSION['user']->isAdmin()) {
    RedirectUtils::Redirect('Menu.php');
    exit;
}

// Set the page title and include HTML header
$sPageTitle = gettext('Church Information');

// Save Settings
if (isset($_POST['save'])) {
   
$cName = $_POST['chu_name'];
$cAddress = $_POST['chu_address'];
$cCity = $_POST['chu_city'];
$cState = $_POST['chu_state'];
$cZip = $_POST['chu_zip'];
$cPhone = $_POST['chu_phone'];
$cEmail = $_POST['chu_email'];
$cSite = $_POST['chu_site'];
$cID = $_POST['chu_ID'];


            $sSQL = "UPDATE church SET chu_Name='".$cName."',".
                        "chu_Address='".$cAddress."',".
                        "chu_City='".$cCity."',".
                        "chu_State='".$cState."',".
                        "chu_Zip='".$cZip."',".
                        "chu_Phone='".$cPhone."',".
                        "chu_Email='".$cEmail."',".
                        "chu_Site='".$cSite."'".

                        
                        
            $sSQL .= " WHERE chu_id = ".$cID;
        
//var_dump($sSQL);
        //Execute the SQL
        RunQuery($sSQL);




    RedirectUtils::Redirect("Churchs.php?saved=true");
}

if (isset($_GET['saved'])) {
    $sGlobalMessage = gettext('Setting saved');
}

require 'Include/Header.php';

// Get settings
?>


<?php 

 //Get the items for this fundraiser
    $sSQL = "SELECT * FROM church";
    $rsDonatedItems = RunQuery($sSQL);

 

    while ($aRow = mysqli_fetch_array($rsDonatedItems)) {
        extract($aRow);
        //echo $chu_id;
      

 ?>

<form method="post">
<div class="table-responsive">
<table class="table table-striped">
<tbody>

<input type="hidden" name="chu_ID" value="<?=$chu_id?>">
<tr>
<td><?= gettext('Church Name')?></td>
<td>
<input type="text" size="40" maxlength="255" name="chu_name" value="<?=$chu_Name ?>" class="form-control">
</td>
</tr>

<tr>
<td><?= gettext('Address')?></td>
<td>
<input type="text" size="40" maxlength="255" name="chu_address" value="<?=$chu_Address ?>" class="form-control">
</td>
</tr>

<tr>
<td><?= gettext('City')?></td>
<td>
<input type="text" size="40" maxlength="255" name="chu_city" value="<?=$chu_City ?>" class="form-control">
</td>
</tr>

<tr>
<td><?= gettext('State')?></td>
<td>
<input type="text" size="40" maxlength="255" name="chu_state" value="<?=$chu_State ?>" class="form-control">
</td>
</tr>

<tr>
<td><?= gettext('Zip')?></td>
<td>
<input type="text" size="40" maxlength="255" name="chu_zip" value="<?=$chu_Zip ?>" class="form-control">
</td>
</tr>

<tr>
<td><?= gettext('Phone')?></td>
<td>
<input type="text" size="40" maxlength="255" name="chu_phone" value="<?=$chu_Phone ?>" class="form-control">
</td>
</tr>


<tr>
<td><?= gettext('Email')?></td>
<td>
<input type="text" size="40" maxlength="255" name="chu_email" value="<?=$chu_Email ?>" class="form-control">
</td>
</tr>

<tr>
<td><?= gettext('Site')?></td>
<td>
<input type="text" size="40" maxlength="255" name="chu_site" value="<?=$chu_Site ?>" class="form-control">
</td>
</tr>


</tbody>
</table>
</div>

<input type='submit' class='btn btn-primary' name='save' id='save' value="<?= gettext('Save Settings') ?>">
</form>

<?php } ?>


        





<?php require 'Include/Footer.php' ?>
