-- --------------------------------------------------------
-- Servidor:                     127.0.0.1
-- Versão do servidor:           10.2.14-MariaDB - mariadb.org binary distribution
-- OS do Servidor:               Win64
-- HeidiSQL Versão:              10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Copiando estrutura para tabela churchcrm_2.master_packages
CREATE TABLE IF NOT EXISTS `master_packages` (
  `package_id` int(11) NOT NULL AUTO_INCREMENT,
  `total_members` int(11) NOT NULL DEFAULT 0,
  `description` varchar(100) DEFAULT NULL,
  `price` decimal(10,0) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `status` int(11) DEFAULT 1,
  PRIMARY KEY (`package_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela churchcrm_2.master_packages: 6 rows
/*!40000 ALTER TABLE `master_packages` DISABLE KEYS */;
INSERT IGNORE INTO `master_packages` (`package_id`, `total_members`, `description`, `price`, `created_at`, `updated_at`, `status`) VALUES
	(1, 0, '1-100', 99, NULL, NULL, 1),
	(2, 0, '100-250', 120, '2019-10-17 00:08:15', '2019-10-17 00:08:15', 1),
	(3, 0, 'asdasd', 444, '2019-10-17 00:11:44', '2019-10-17 00:11:44', 1),
	(4, 0, 'Aluguel', 455, '2019-10-17 00:12:27', '2019-10-17 22:52:35', 1),
	(5, 0, 'Formácia ', 545, '2019-10-17 00:12:45', '2019-10-17 00:29:26', 1),
	(6, 0, 'Formácia popular3', 545, '2019-10-17 00:12:49', '2019-10-17 00:29:11', 1);
/*!40000 ALTER TABLE `master_packages` ENABLE KEYS */;

-- Copiando estrutura para tabela churchcrm_2.master_user
CREATE TABLE IF NOT EXISTS `master_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(250) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela churchcrm_2.master_user: 1 rows
/*!40000 ALTER TABLE `master_user` DISABLE KEYS */;
INSERT IGNORE INTO `master_user` (`id`, `name`, `email`, `username`, `password`, `created_at`, `updated_at`) VALUES
	(1, 'Master', 'master@mail.com', 'admin', '0878baf6d9bfa61520803ce72bb7d934bec99b77d31f17fe9d5dbb89d33d785f', '2019-10-16 21:47:56', '2019-10-16 21:47:58');
/*!40000 ALTER TABLE `master_user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
