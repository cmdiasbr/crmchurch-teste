-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: 19-Set-2019 às 16:47
-- Versão do servidor: 5.7.26
-- versão do PHP: 7.0.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `churchcrm_2`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `church`
--

DROP TABLE IF EXISTS `church`;
CREATE TABLE IF NOT EXISTS `church` (
  `chu_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `chu_Name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `chu_Address` text COLLATE utf8_unicode_ci,
  `chu_City` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `chu_State` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `chu_Zip` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `chu_Phone` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `chu_Email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `chu_Site` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `chu_Parent` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`chu_id`),
  KEY `chu_id` (`chu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `church`
--

INSERT INTO `church` (`chu_id`, `chu_Name`, `chu_Address`, `chu_City`, `chu_State`, `chu_Zip`, `chu_Phone`, `chu_Email`, `chu_Site`, `chu_Parent`) VALUES
(9, 'Church test', 'street 01', 'Miami', 'Forida', '123456', '123456', 'chur@g.com', '', NULL);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
