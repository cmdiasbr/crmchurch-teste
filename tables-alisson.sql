-- --------------------------------------------------------
-- Servidor:                     127.0.0.1
-- Versão do servidor:           10.2.14-MariaDB - mariadb.org binary distribution
-- OS do Servidor:               Win64
-- HeidiSQL Versão:              10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Copiando estrutura para tabela churchcrm_2.business_category
CREATE TABLE IF NOT EXISTS `business_category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela churchcrm_2.business_category: 2 rows
/*!40000 ALTER TABLE `business_category` DISABLE KEYS */;
INSERT IGNORE INTO `business_category` (`category_id`, `name`) VALUES
	(1, 'Oficina'),
	(2, 'Pet Shop');
/*!40000 ALTER TABLE `business_category` ENABLE KEYS */;

-- Copiando estrutura para tabela churchcrm_2.person_business
CREATE TABLE IF NOT EXISTS `person_business` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT '',
  `description` varchar(250) DEFAULT '',
  `phone` varchar(250) DEFAULT '',
  `logo` varchar(250) DEFAULT '',
  `site` varchar(250) DEFAULT '',
  `person_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela churchcrm_2.person_business: 1 rows
/*!40000 ALTER TABLE `person_business` DISABLE KEYS */;
INSERT IGNORE INTO `person_business` (`id`, `name`, `description`, `phone`, `logo`, `site`, `person_id`, `category_id`) VALUES
	(1, 'FRANCISCO MARQUES DE ARAUJO', 'Formácia popular', '8999212842', NULL, 'www.mybusiness.me', 2, 2);
/*!40000 ALTER TABLE `person_business` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
