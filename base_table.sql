-- --------------------------------------------------------
-- Servidor:                     127.0.0.1
-- Versão do servidor:           10.2.14-MariaDB - mariadb.org binary distribution
-- OS do Servidor:               Win64
-- HeidiSQL Versão:              10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Copiando estrutura para tabela churchcrm_2.finance
CREATE TABLE IF NOT EXISTS `finance` (
  `finance_id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(250) DEFAULT NULL,
  `amount` double NOT NULL,
  `church_id` int(11) NOT NULL DEFAULT 0,
  `category_id` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` datetime DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`finance_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela churchcrm_2.finance: 0 rows
/*!40000 ALTER TABLE `finance` DISABLE KEYS */;
INSERT IGNORE INTO `finance` (`finance_id`, `description`, `amount`, `church_id`, `category_id`, `type`, `status`, `date`, `created_at`, `updated_at`) VALUES
	(1, 'Pagamento Serviço de poda', 450, 0, 5, 1, 1, '2019-09-30 00:00:00', '2019-09-30 23:37:03', '2019-09-30 23:37:03');
/*!40000 ALTER TABLE `finance` ENABLE KEYS */;

-- Copiando estrutura para tabela churchcrm_2.finance_category
CREATE TABLE IF NOT EXISTS `finance_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `church_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela churchcrm_2.finance_category: 2 rows
/*!40000 ALTER TABLE `finance_category` DISABLE KEYS */;
INSERT IGNORE INTO `finance_category` (`id`, `name`, `church_id`) VALUES
	(1, 'Aluguel', NULL),
	(4, 'Gasolina', NULL),
	(5, 'Divesas', NULL);
/*!40000 ALTER TABLE `finance_category` ENABLE KEYS */;

-- Copiando estrutura para tabela churchcrm_2.person_status
CREATE TABLE IF NOT EXISTS `person_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela churchcrm_2.person_status: 5 rows
/*!40000 ALTER TABLE `person_status` DISABLE KEYS */;
INSERT IGNORE INTO `person_status` (`id`, `description`) VALUES
	(1, 'Active'),
	(6, 'Outro'),
	(4, 'Inactive'),
	(7, 'Aluguel'),
	(8, 'Aluguel');
/*!40000 ALTER TABLE `person_status` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
