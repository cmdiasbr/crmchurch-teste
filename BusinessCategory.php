<?php
/*******************************************************************************
 *
 *  filename    : PersonNewBusiness.php
 *  website     : http://www.churchcrm.io
 *  copyright   : Copyright 2001, 2002, 2003 Deane Barker, Chris Gebhardt
 *                Copyright 2004-2005 Michael Wilt
  *
 ******************************************************************************/

//Include the function library
require 'Include/Config.php';
require 'Include/Functions.php';

use ChurchCRM\dto\SystemConfig;

use ChurchCRM\Utils\RedirectUtils;
use Propel\Runtime\Propel;
require_once './classes/BusinessCategory.php';
//Set the page title
$sPageTitle = gettext('Business Category');

define("DS", DIRECTORY_SEPARATOR);
define("VIEW", dirname(__FILE__).DS.'views');

$busness = new \classes\BusinessCategory();
require 'Include/Header.php';

if(isset($_GET['method']) && method_exists($busness, $_GET['method'])){
    $method =  $_GET['method'];
    $busness->$method();
} else {
    $busness->ListCategory();
}





require 'Include/Footer.php' ;