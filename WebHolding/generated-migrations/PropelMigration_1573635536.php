<?php

use Propel\Generator\Manager\MigrationManager;

/**
 * Data object containing the SQL and PHP code to migrate the database
 * up to version 1573635536.
 * Generated on 2019-11-13 08:58:56 by alyssondaniel
 */
class PropelMigration_1573635536
{
    public $comment = '';

    public function preUp(MigrationManager $manager)
    {
        // add the pre-migration code here
    }

    public function postUp(MigrationManager $manager)
    {
        // add the post-migration code here
    }

    public function preDown(MigrationManager $manager)
    {
        // add the pre-migration code here
    }

    public function postDown(MigrationManager $manager)
    {
        // add the post-migration code here
    }

    /**
     * Get the SQL statements for the Up migration
     *
     * @return array list of the SQL strings to execute for the Up migration
     *               the keys being the datasources
     */
    public function getUpSQL()
    {
        return array (
            'church' => "CREATE TABLE `business_category` (
              `category_id` int(11) NOT NULL AUTO_INCREMENT,
              `name` varchar(150) DEFAULT NULL,
              `bus_chu_id` int(11) DEFAULT NULL,
              PRIMARY KEY (`category_id`)
            ) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;",
        );
    }

    /**
     * Get the SQL statements for the Down migration
     *
     * @return array list of the SQL strings to execute for the Down migration
     *               the keys being the datasources
     */
    public function getDownSQL()
    {
        return array (
            'church' => 'DROP TABLE IF EXISTS `business_category`',
        );
    }

}
