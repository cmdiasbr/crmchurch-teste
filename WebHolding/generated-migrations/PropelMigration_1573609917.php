<?php

use Propel\Generator\Manager\MigrationManager;

/**
 * Data object containing the SQL and PHP code to migrate the database
 * up to version 1573609917.
 * Generated on 2019-11-13 01:51:57 by alyssondaniel
 */
class PropelMigration_1573609917
{
    public $comment = '';

    public function preUp(MigrationManager $manager)
    {
        // add the pre-migration code here
    }

    public function postUp(MigrationManager $manager)
    {
        // add the post-migration code here
    }

    public function preDown(MigrationManager $manager)
    {
        // add the pre-migration code here
    }

    public function postDown(MigrationManager $manager)
    {
        // add the post-migration code here
    }

    /**
     * Get the SQL statements for the Up migration
     *
     * @return array list of the SQL strings to execute for the Up migration
     *               the keys being the datasources
     */
    public function getUpSQL()
    {
        return array (
          'church' => "CREATE TABLE `person_business` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `name` varchar(250) DEFAULT '',
          `description` varchar(250) DEFAULT '',
          `phone` varchar(250) DEFAULT '',
          `logo` varchar(250) DEFAULT '',
          `site` varchar(250) DEFAULT '',
          `person_id` int(11) DEFAULT NULL,
          `category_id` int(11) DEFAULT NULL,
          `bus_chu_ID` int(11) DEFAULT '0',
          PRIMARY KEY (`id`)
        ) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1",
      );
    }

    /**
     * Get the SQL statements for the Down migration
     *
     * @return array list of the SQL strings to execute for the Down migration
     *               the keys being the datasources
     */
    public function getDownSQL()
    {
        return array (
          'church' => 'DROP TABLE IF EXISTS `person_business`',
        );
    }

}
