-- --------------------------------------------------------
-- Servidor:                     127.0.0.1
-- Versão do servidor:           10.2.14-MariaDB - mariadb.org binary distribution
-- OS do Servidor:               Win64
-- HeidiSQL Versão:              10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Copiando estrutura para tabela churchcrm_2.business_category
CREATE TABLE IF NOT EXISTS `business_category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela churchcrm_2.business_category: 2 rows
/*!40000 ALTER TABLE `business_category` DISABLE KEYS */;
INSERT IGNORE INTO `business_category` (`category_id`, `name`) VALUES
	(1, 'Oficina'),
	(2, 'Pet Shop');
/*!40000 ALTER TABLE `business_category` ENABLE KEYS */;

-- Copiando estrutura para tabela churchcrm_2.calendars
CREATE TABLE IF NOT EXISTS `calendars` (
  `calendar_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `accesstoken` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `foregroundColor` varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `backgroundColor` varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`calendar_id`),
  UNIQUE KEY `accesstoken` (`accesstoken`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Copiando dados para a tabela churchcrm_2.calendars: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `calendars` DISABLE KEYS */;
INSERT IGNORE INTO `calendars` (`calendar_id`, `name`, `accesstoken`, `foregroundColor`, `backgroundColor`) VALUES
	(1, 'Public Calendar', NULL, 'FFFFFF', '00AA00'),
	(2, 'Private Calendar', NULL, 'FFFFFF', '0000AA');
/*!40000 ALTER TABLE `calendars` ENABLE KEYS */;

-- Copiando estrutura para tabela churchcrm_2.calendar_events
CREATE TABLE IF NOT EXISTS `calendar_events` (
  `calendar_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  PRIMARY KEY (`calendar_id`,`event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Copiando dados para a tabela churchcrm_2.calendar_events: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `calendar_events` DISABLE KEYS */;
/*!40000 ALTER TABLE `calendar_events` ENABLE KEYS */;

-- Copiando estrutura para tabela churchcrm_2.canvassdata_can
CREATE TABLE IF NOT EXISTS `canvassdata_can` (
  `can_ID` mediumint(9) unsigned NOT NULL AUTO_INCREMENT,
  `can_famID` mediumint(9) NOT NULL DEFAULT 0,
  `can_Canvasser` mediumint(9) NOT NULL DEFAULT 0,
  `can_FYID` mediumint(9) DEFAULT NULL,
  `can_date` date DEFAULT NULL,
  `can_Positive` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `can_Critical` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `can_Insightful` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `can_Financial` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `can_Suggestion` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `can_NotInterested` tinyint(1) NOT NULL DEFAULT 0,
  `can_WhyNotInterested` text COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`can_ID`),
  UNIQUE KEY `can_ID` (`can_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Copiando dados para a tabela churchcrm_2.canvassdata_can: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `canvassdata_can` DISABLE KEYS */;
/*!40000 ALTER TABLE `canvassdata_can` ENABLE KEYS */;

-- Copiando estrutura para tabela churchcrm_2.church_location_person
CREATE TABLE IF NOT EXISTS `church_location_person` (
  `location_id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `order` int(11) NOT NULL,
  `person_location_role_id` int(11) NOT NULL,
  PRIMARY KEY (`location_id`,`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Copiando dados para a tabela churchcrm_2.church_location_person: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `church_location_person` DISABLE KEYS */;
/*!40000 ALTER TABLE `church_location_person` ENABLE KEYS */;

-- Copiando estrutura para tabela churchcrm_2.church_location_role
CREATE TABLE IF NOT EXISTS `church_location_role` (
  `location_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `role_order` int(11) NOT NULL,
  `role_title` int(11) NOT NULL,
  PRIMARY KEY (`location_id`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Copiando dados para a tabela churchcrm_2.church_location_role: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `church_location_role` DISABLE KEYS */;
/*!40000 ALTER TABLE `church_location_role` ENABLE KEYS */;

-- Copiando estrutura para tabela churchcrm_2.config_cfg
CREATE TABLE IF NOT EXISTS `config_cfg` (
  `cfg_id` int(11) NOT NULL DEFAULT 0,
  `cfg_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `cfg_value` text COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`cfg_id`),
  UNIQUE KEY `cfg_name` (`cfg_name`),
  KEY `cfg_id` (`cfg_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Copiando dados para a tabela churchcrm_2.config_cfg: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `config_cfg` DISABLE KEYS */;
INSERT IGNORE INTO `config_cfg` (`cfg_id`, `cfg_name`, `cfg_value`) VALUES
	(1046, 'sLastIntegrityCheckTimeStamp', '20190916-200508');
/*!40000 ALTER TABLE `config_cfg` ENABLE KEYS */;

-- Copiando estrutura para tabela churchcrm_2.deposit_dep
CREATE TABLE IF NOT EXISTS `deposit_dep` (
  `dep_ID` mediumint(9) unsigned NOT NULL AUTO_INCREMENT,
  `dep_Date` date DEFAULT NULL,
  `dep_Comment` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `dep_EnteredBy` mediumint(9) unsigned DEFAULT NULL,
  `dep_Closed` tinyint(1) NOT NULL DEFAULT 0,
  `dep_Type` enum('Bank','CreditCard','BankDraft','eGive') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Bank',
  PRIMARY KEY (`dep_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci PACK_KEYS=0;

-- Copiando dados para a tabela churchcrm_2.deposit_dep: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `deposit_dep` DISABLE KEYS */;
/*!40000 ALTER TABLE `deposit_dep` ENABLE KEYS */;

-- Copiando estrutura para tabela churchcrm_2.donateditem_di
CREATE TABLE IF NOT EXISTS `donateditem_di` (
  `di_ID` mediumint(9) unsigned NOT NULL AUTO_INCREMENT,
  `di_item` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `di_FR_ID` mediumint(9) unsigned NOT NULL,
  `di_donor_ID` mediumint(9) NOT NULL DEFAULT 0,
  `di_buyer_ID` mediumint(9) NOT NULL DEFAULT 0,
  `di_multibuy` smallint(1) NOT NULL DEFAULT 0,
  `di_title` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `di_description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `di_sellprice` decimal(8,2) DEFAULT NULL,
  `di_estprice` decimal(8,2) DEFAULT NULL,
  `di_minimum` decimal(8,2) DEFAULT NULL,
  `di_materialvalue` decimal(8,2) DEFAULT NULL,
  `di_EnteredBy` smallint(5) unsigned NOT NULL DEFAULT 0,
  `di_EnteredDate` date NOT NULL,
  `di_picture` text COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`di_ID`),
  UNIQUE KEY `di_ID` (`di_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Copiando dados para a tabela churchcrm_2.donateditem_di: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `donateditem_di` DISABLE KEYS */;
/*!40000 ALTER TABLE `donateditem_di` ENABLE KEYS */;

-- Copiando estrutura para tabela churchcrm_2.donationfund_fun
CREATE TABLE IF NOT EXISTS `donationfund_fun` (
  `fun_ID` tinyint(3) NOT NULL AUTO_INCREMENT,
  `fun_Active` enum('true','false') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'true',
  `fun_Name` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fun_Description` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`fun_ID`),
  UNIQUE KEY `fun_ID` (`fun_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Copiando dados para a tabela churchcrm_2.donationfund_fun: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `donationfund_fun` DISABLE KEYS */;
INSERT IGNORE INTO `donationfund_fun` (`fun_ID`, `fun_Active`, `fun_Name`, `fun_Description`) VALUES
	(1, 'true', 'Pledges', 'Pledge income for the operating budget');
/*!40000 ALTER TABLE `donationfund_fun` ENABLE KEYS */;

-- Copiando estrutura para tabela churchcrm_2.egive_egv
CREATE TABLE IF NOT EXISTS `egive_egv` (
  `egv_egiveID` varchar(16) CHARACTER SET utf8 NOT NULL,
  `egv_famID` int(11) NOT NULL,
  `egv_DateEntered` datetime NOT NULL,
  `egv_DateLastEdited` datetime NOT NULL,
  `egv_EnteredBy` smallint(6) NOT NULL DEFAULT 0,
  `egv_EditedBy` smallint(6) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Copiando dados para a tabela churchcrm_2.egive_egv: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `egive_egv` DISABLE KEYS */;
/*!40000 ALTER TABLE `egive_egv` ENABLE KEYS */;

-- Copiando estrutura para view churchcrm_2.email_count
-- Criando tabela temporária para evitar erros de dependência de VIEW
CREATE TABLE `email_count` (
	`email` VARCHAR(100) NULL COLLATE 'utf8_unicode_ci',
	`total` BIGINT(21) NOT NULL
) ENGINE=MyISAM;

-- Copiando estrutura para view churchcrm_2.email_list
-- Criando tabela temporária para evitar erros de dependência de VIEW
CREATE TABLE `email_list` (
	`email` VARCHAR(100) NULL COLLATE 'utf8_unicode_ci',
	`type` VARCHAR(11) NOT NULL COLLATE 'utf8mb4_general_ci',
	`id` MEDIUMINT(9) UNSIGNED NOT NULL
) ENGINE=MyISAM;

-- Copiando estrutura para tabela churchcrm_2.email_message_pending_emp
CREATE TABLE IF NOT EXISTS `email_message_pending_emp` (
  `emp_usr_id` mediumint(9) unsigned NOT NULL DEFAULT 0,
  `emp_to_send` smallint(5) unsigned NOT NULL DEFAULT 0,
  `emp_subject` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `emp_message` text COLLATE utf8_unicode_ci NOT NULL,
  `emp_attach_name` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `emp_attach` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Copiando dados para a tabela churchcrm_2.email_message_pending_emp: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `email_message_pending_emp` DISABLE KEYS */;
/*!40000 ALTER TABLE `email_message_pending_emp` ENABLE KEYS */;

-- Copiando estrutura para tabela churchcrm_2.email_recipient_pending_erp
CREATE TABLE IF NOT EXISTS `email_recipient_pending_erp` (
  `erp_id` smallint(5) unsigned NOT NULL DEFAULT 0,
  `erp_usr_id` mediumint(9) unsigned NOT NULL DEFAULT 0,
  `erp_num_attempt` smallint(5) unsigned NOT NULL DEFAULT 0,
  `erp_failed_time` datetime DEFAULT NULL,
  `erp_email_address` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Copiando dados para a tabela churchcrm_2.email_recipient_pending_erp: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `email_recipient_pending_erp` DISABLE KEYS */;
/*!40000 ALTER TABLE `email_recipient_pending_erp` ENABLE KEYS */;

-- Copiando estrutura para tabela churchcrm_2.eventcountnames_evctnm
CREATE TABLE IF NOT EXISTS `eventcountnames_evctnm` (
  `evctnm_countid` int(5) NOT NULL AUTO_INCREMENT,
  `evctnm_eventtypeid` smallint(5) NOT NULL DEFAULT 0,
  `evctnm_countname` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `evctnm_notes` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  UNIQUE KEY `evctnm_countid` (`evctnm_countid`),
  UNIQUE KEY `evctnm_eventtypeid` (`evctnm_eventtypeid`,`evctnm_countname`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Copiando dados para a tabela churchcrm_2.eventcountnames_evctnm: ~6 rows (aproximadamente)
/*!40000 ALTER TABLE `eventcountnames_evctnm` DISABLE KEYS */;
INSERT IGNORE INTO `eventcountnames_evctnm` (`evctnm_countid`, `evctnm_eventtypeid`, `evctnm_countname`, `evctnm_notes`) VALUES
	(1, 1, 'Total', ''),
	(2, 1, 'Members', ''),
	(3, 1, 'Visitors', ''),
	(4, 2, 'Total', ''),
	(5, 2, 'Members', ''),
	(6, 2, 'Visitors', '');
/*!40000 ALTER TABLE `eventcountnames_evctnm` ENABLE KEYS */;

-- Copiando estrutura para tabela churchcrm_2.eventcounts_evtcnt
CREATE TABLE IF NOT EXISTS `eventcounts_evtcnt` (
  `evtcnt_eventid` int(5) NOT NULL DEFAULT 0,
  `evtcnt_countid` int(5) NOT NULL DEFAULT 0,
  `evtcnt_countname` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `evtcnt_countcount` int(6) DEFAULT NULL,
  `evtcnt_notes` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`evtcnt_eventid`,`evtcnt_countid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Copiando dados para a tabela churchcrm_2.eventcounts_evtcnt: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `eventcounts_evtcnt` DISABLE KEYS */;
/*!40000 ALTER TABLE `eventcounts_evtcnt` ENABLE KEYS */;

-- Copiando estrutura para tabela churchcrm_2.events_event
CREATE TABLE IF NOT EXISTS `events_event` (
  `event_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_type` int(11) NOT NULL DEFAULT 0,
  `event_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `event_desc` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `event_text` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `event_start` datetime NOT NULL,
  `event_end` datetime NOT NULL,
  `inactive` int(1) NOT NULL DEFAULT 0,
  `event_typename` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `location_id` int(11) DEFAULT NULL,
  `primary_contact_person_id` int(11) DEFAULT NULL,
  `secondary_contact_person_id` int(11) DEFAULT NULL,
  `event_url` text COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Copiando dados para a tabela churchcrm_2.events_event: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `events_event` DISABLE KEYS */;
/*!40000 ALTER TABLE `events_event` ENABLE KEYS */;

-- Copiando estrutura para tabela churchcrm_2.event_attend
CREATE TABLE IF NOT EXISTS `event_attend` (
  `attend_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` int(11) NOT NULL DEFAULT 0,
  `person_id` int(11) NOT NULL DEFAULT 0,
  `checkin_date` datetime DEFAULT NULL,
  `checkin_id` int(11) DEFAULT NULL,
  `checkout_date` datetime DEFAULT NULL,
  `checkout_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`attend_id`),
  UNIQUE KEY `event_id` (`event_id`,`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Copiando dados para a tabela churchcrm_2.event_attend: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `event_attend` DISABLE KEYS */;
/*!40000 ALTER TABLE `event_attend` ENABLE KEYS */;

-- Copiando estrutura para tabela churchcrm_2.event_audience
CREATE TABLE IF NOT EXISTS `event_audience` (
  `event_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`event_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Copiando dados para a tabela churchcrm_2.event_audience: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `event_audience` DISABLE KEYS */;
/*!40000 ALTER TABLE `event_audience` ENABLE KEYS */;

-- Copiando estrutura para tabela churchcrm_2.event_types
CREATE TABLE IF NOT EXISTS `event_types` (
  `type_id` int(11) NOT NULL AUTO_INCREMENT,
  `type_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `type_defstarttime` time NOT NULL DEFAULT '00:00:00',
  `type_defrecurtype` enum('none','weekly','monthly','yearly') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'none',
  `type_defrecurDOW` enum('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Sunday',
  `type_defrecurDOM` char(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `type_defrecurDOY` date NOT NULL DEFAULT '2000-01-01',
  `type_active` int(1) NOT NULL DEFAULT 1,
  `type_grpid` mediumint(9) DEFAULT NULL,
  PRIMARY KEY (`type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Copiando dados para a tabela churchcrm_2.event_types: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `event_types` DISABLE KEYS */;
INSERT IGNORE INTO `event_types` (`type_id`, `type_name`, `type_defstarttime`, `type_defrecurtype`, `type_defrecurDOW`, `type_defrecurDOM`, `type_defrecurDOY`, `type_active`, `type_grpid`) VALUES
	(1, 'Church Service', '10:30:00', 'weekly', 'Sunday', '', '2016-01-01', 1, NULL),
	(2, 'Sunday School', '09:30:00', 'weekly', 'Sunday', '', '2016-01-01', 1, NULL);
/*!40000 ALTER TABLE `event_types` ENABLE KEYS */;

-- Copiando estrutura para tabela churchcrm_2.family_custom
CREATE TABLE IF NOT EXISTS `family_custom` (
  `fam_ID` mediumint(9) NOT NULL DEFAULT 0,
  PRIMARY KEY (`fam_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Copiando dados para a tabela churchcrm_2.family_custom: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `family_custom` DISABLE KEYS */;
INSERT IGNORE INTO `family_custom` (`fam_ID`) VALUES
	(1);
/*!40000 ALTER TABLE `family_custom` ENABLE KEYS */;

-- Copiando estrutura para tabela churchcrm_2.family_custom_master
CREATE TABLE IF NOT EXISTS `family_custom_master` (
  `fam_custom_Order` smallint(6) NOT NULL DEFAULT 0,
  `fam_custom_Field` varchar(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `fam_custom_Name` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `fam_custom_Special` mediumint(8) unsigned DEFAULT NULL,
  `fam_custom_Side` enum('left','right') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'left',
  `fam_custom_FieldSec` tinyint(4) NOT NULL DEFAULT 1,
  `type_ID` tinyint(4) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Copiando dados para a tabela churchcrm_2.family_custom_master: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `family_custom_master` DISABLE KEYS */;
/*!40000 ALTER TABLE `family_custom_master` ENABLE KEYS */;

-- Copiando estrutura para tabela churchcrm_2.family_fam
CREATE TABLE IF NOT EXISTS `family_fam` (
  `fam_ID` mediumint(9) unsigned NOT NULL AUTO_INCREMENT,
  `fam_Name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fam_Address1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fam_Address2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fam_City` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fam_State` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fam_Zip` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fam_Country` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fam_HomePhone` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fam_WorkPhone` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fam_CellPhone` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fam_Email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fam_WeddingDate` date DEFAULT NULL,
  `fam_DateEntered` datetime NOT NULL,
  `fam_DateLastEdited` datetime DEFAULT NULL,
  `fam_EnteredBy` smallint(5) NOT NULL DEFAULT 0,
  `fam_EditedBy` smallint(5) unsigned DEFAULT 0,
  `fam_scanCheck` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `fam_scanCredit` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `fam_SendNewsLetter` enum('FALSE','TRUE') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'FALSE',
  `fam_DateDeactivated` date DEFAULT NULL,
  `fam_OkToCanvass` enum('FALSE','TRUE') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'FALSE',
  `fam_Canvasser` smallint(5) unsigned NOT NULL DEFAULT 0,
  `fam_Latitude` double DEFAULT NULL,
  `fam_Longitude` double DEFAULT NULL,
  `fam_Envelope` mediumint(9) NOT NULL DEFAULT 0,
  PRIMARY KEY (`fam_ID`),
  KEY `fam_ID` (`fam_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Copiando dados para a tabela churchcrm_2.family_fam: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `family_fam` DISABLE KEYS */;
INSERT IGNORE INTO `family_fam` (`fam_ID`, `fam_Name`, `fam_Address1`, `fam_Address2`, `fam_City`, `fam_State`, `fam_Zip`, `fam_Country`, `fam_HomePhone`, `fam_WorkPhone`, `fam_CellPhone`, `fam_Email`, `fam_WeddingDate`, `fam_DateEntered`, `fam_DateLastEdited`, `fam_EnteredBy`, `fam_EditedBy`, `fam_scanCheck`, `fam_scanCredit`, `fam_SendNewsLetter`, `fam_DateDeactivated`, `fam_OkToCanvass`, `fam_Canvasser`, `fam_Latitude`, `fam_Longitude`, `fam_Envelope`) VALUES
	(1, 'Façanha Araújo', 'Rua Francisco Lins da Trindade', '', 'Teresina', 'Piauí', '64063073', 'Brazil (Brasil)', '(869) 981-7647', '', '', '', NULL, '2019-09-02 20:08:05', NULL, 1, 0, NULL, NULL, 'TRUE', NULL, 'TRUE', 0, 0, 0, 0);
/*!40000 ALTER TABLE `family_fam` ENABLE KEYS */;

-- Copiando estrutura para tabela churchcrm_2.fundraiser_fr
CREATE TABLE IF NOT EXISTS `fundraiser_fr` (
  `fr_ID` mediumint(9) unsigned NOT NULL AUTO_INCREMENT,
  `fr_date` date DEFAULT NULL,
  `fr_title` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `fr_description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `fr_EnteredBy` smallint(5) unsigned NOT NULL DEFAULT 0,
  `fr_EnteredDate` date NOT NULL,
  PRIMARY KEY (`fr_ID`),
  UNIQUE KEY `fr_ID` (`fr_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Copiando dados para a tabela churchcrm_2.fundraiser_fr: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `fundraiser_fr` DISABLE KEYS */;
/*!40000 ALTER TABLE `fundraiser_fr` ENABLE KEYS */;

-- Copiando estrutura para tabela churchcrm_2.groupprop_master
CREATE TABLE IF NOT EXISTS `groupprop_master` (
  `grp_ID` mediumint(9) unsigned NOT NULL DEFAULT 0,
  `prop_ID` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `prop_Field` varchar(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `prop_Name` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `prop_Description` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type_ID` smallint(5) unsigned NOT NULL DEFAULT 0,
  `prop_Special` mediumint(9) unsigned DEFAULT NULL,
  `prop_PersonDisplay` enum('false','true') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'false'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Group-specific properties order, name, description, type';

-- Copiando dados para a tabela churchcrm_2.groupprop_master: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `groupprop_master` DISABLE KEYS */;
/*!40000 ALTER TABLE `groupprop_master` ENABLE KEYS */;

-- Copiando estrutura para tabela churchcrm_2.group_grp
CREATE TABLE IF NOT EXISTS `group_grp` (
  `grp_ID` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `grp_Type` tinyint(4) NOT NULL DEFAULT 0,
  `grp_RoleListID` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `grp_DefaultRole` mediumint(9) NOT NULL DEFAULT 0,
  `grp_Name` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `grp_Description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `grp_hasSpecialProps` tinyint(1) NOT NULL DEFAULT 0,
  `grp_active` tinyint(1) NOT NULL DEFAULT 1,
  `grp_include_email_export` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`grp_ID`),
  UNIQUE KEY `grp_ID` (`grp_ID`),
  KEY `grp_ID_2` (`grp_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Copiando dados para a tabela churchcrm_2.group_grp: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `group_grp` DISABLE KEYS */;
/*!40000 ALTER TABLE `group_grp` ENABLE KEYS */;

-- Copiando estrutura para tabela churchcrm_2.istlookup_lu
CREATE TABLE IF NOT EXISTS `istlookup_lu` (
  `lu_fam_ID` mediumint(9) NOT NULL DEFAULT 0,
  `lu_LookupDateTime` datetime NOT NULL DEFAULT '2000-01-01 00:00:00',
  `lu_DeliveryLine1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lu_DeliveryLine2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lu_City` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lu_State` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lu_ZipAddon` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lu_Zip` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lu_Addon` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lu_LOTNumber` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lu_DPCCheckdigit` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lu_RecordType` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lu_LastLine` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lu_CarrierRoute` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lu_ReturnCodes` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lu_ErrorCodes` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lu_ErrorDesc` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`lu_fam_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='US Address Verification Lookups From Intelligent Search Tech';

-- Copiando dados para a tabela churchcrm_2.istlookup_lu: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `istlookup_lu` DISABLE KEYS */;
/*!40000 ALTER TABLE `istlookup_lu` ENABLE KEYS */;

-- Copiando estrutura para tabela churchcrm_2.kioskassginment_kasm
CREATE TABLE IF NOT EXISTS `kioskassginment_kasm` (
  `kasm_ID` mediumint(9) unsigned NOT NULL AUTO_INCREMENT,
  `kasm_kdevId` mediumint(9) DEFAULT NULL,
  `kasm_AssignmentType` mediumint(9) DEFAULT NULL,
  `kasm_EventId` mediumint(9) DEFAULT 0,
  PRIMARY KEY (`kasm_ID`),
  UNIQUE KEY `kasm_ID` (`kasm_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Copiando dados para a tabela churchcrm_2.kioskassginment_kasm: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `kioskassginment_kasm` DISABLE KEYS */;
/*!40000 ALTER TABLE `kioskassginment_kasm` ENABLE KEYS */;

-- Copiando estrutura para tabela churchcrm_2.kioskdevice_kdev
CREATE TABLE IF NOT EXISTS `kioskdevice_kdev` (
  `kdev_ID` mediumint(9) unsigned NOT NULL AUTO_INCREMENT,
  `kdev_GUIDHash` char(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kdev_Name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kdev_deviceType` mediumint(9) NOT NULL DEFAULT 0,
  `kdev_lastHeartbeat` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `kdev_Accepted` tinyint(1) DEFAULT NULL,
  `kdev_PendingCommands` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`kdev_ID`),
  UNIQUE KEY `kdev_ID` (`kdev_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Copiando dados para a tabela churchcrm_2.kioskdevice_kdev: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `kioskdevice_kdev` DISABLE KEYS */;
/*!40000 ALTER TABLE `kioskdevice_kdev` ENABLE KEYS */;

-- Copiando estrutura para tabela churchcrm_2.list_lst
CREATE TABLE IF NOT EXISTS `list_lst` (
  `lst_ID` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `lst_OptionID` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `lst_OptionSequence` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `lst_OptionName` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Copiando dados para a tabela churchcrm_2.list_lst: ~44 rows (aproximadamente)
/*!40000 ALTER TABLE `list_lst` DISABLE KEYS */;
INSERT IGNORE INTO `list_lst` (`lst_ID`, `lst_OptionID`, `lst_OptionSequence`, `lst_OptionName`) VALUES
	(1, 1, 2, 'Member'),
	(1, 2, 1, 'Regular Attender'),
	(1, 3, 4, 'Guest'),
	(1, 5, 3, 'Non-Attender'),
	(1, 4, 5, 'Non-Attender (staff)'),
	(2, 1, 1, 'Head of Household'),
	(2, 2, 2, 'Spouse'),
	(2, 3, 3, 'Child'),
	(2, 4, 4, 'Other Relative'),
	(2, 5, 5, 'Non Relative'),
	(3, 1, 1, 'Ministry'),
	(3, 2, 2, 'Team'),
	(3, 3, 3, 'Bible Study'),
	(3, 4, 4, 'Sunday School Class'),
	(4, 1, 1, 'True / False'),
	(4, 2, 2, 'Date'),
	(4, 3, 3, 'Text Field (50 char)'),
	(4, 4, 4, 'Text Field (100 char)'),
	(4, 5, 5, 'Text Field (Long)'),
	(4, 6, 6, 'Year'),
	(4, 7, 7, 'Season'),
	(4, 8, 8, 'Number'),
	(4, 9, 9, 'Person from Group'),
	(4, 10, 10, 'Money'),
	(4, 11, 11, 'Phone Number'),
	(4, 12, 12, 'Custom Drop-Down List'),
	(5, 1, 1, 'bAll'),
	(5, 2, 2, 'bAdmin'),
	(5, 3, 3, 'bAddRecords'),
	(5, 4, 4, 'bEditRecords'),
	(5, 5, 5, 'bDeleteRecords'),
	(5, 6, 6, 'bMenuOptions'),
	(5, 7, 7, 'bManageGroups'),
	(5, 8, 8, 'bFinance'),
	(5, 9, 9, 'bNotes'),
	(5, 10, 10, 'bCommunication'),
	(5, 11, 11, 'bCanvasser'),
	(10, 1, 1, 'Teacher'),
	(10, 2, 2, 'Student'),
	(11, 1, 1, 'Member'),
	(12, 1, 1, 'Teacher'),
	(12, 2, 2, 'Student'),
	(13, 1, 1, 'Inativo'),
	(13, 2, 2, 'Falecido'),
	(13, 3, 3, 'Ativo'),
	(13, 4, 4, 'Tranferido'),
	(1, 6, 6, 'presbitro');
/*!40000 ALTER TABLE `list_lst` ENABLE KEYS */;

-- Copiando estrutura para tabela churchcrm_2.locations
CREATE TABLE IF NOT EXISTS `locations` (
  `location_id` int(11) NOT NULL,
  `location_typeId` int(11) NOT NULL,
  `location_name` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `location_address` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `location_city` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `location_state` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `location_zip` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `location_country` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `location_phone` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location_email` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location_timzezone` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`location_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Copiando dados para a tabela churchcrm_2.locations: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `locations` DISABLE KEYS */;
/*!40000 ALTER TABLE `locations` ENABLE KEYS */;

-- Copiando estrutura para tabela churchcrm_2.menu_links
CREATE TABLE IF NOT EXISTS `menu_links` (
  `linkId` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `linkName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `linkUri` text COLLATE utf8_unicode_ci NOT NULL,
  `linkOrder` int(11) NOT NULL,
  PRIMARY KEY (`linkId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Copiando dados para a tabela churchcrm_2.menu_links: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `menu_links` DISABLE KEYS */;
/*!40000 ALTER TABLE `menu_links` ENABLE KEYS */;

-- Copiando estrutura para tabela churchcrm_2.multibuy_mb
CREATE TABLE IF NOT EXISTS `multibuy_mb` (
  `mb_ID` mediumint(9) unsigned NOT NULL AUTO_INCREMENT,
  `mb_per_ID` mediumint(9) NOT NULL DEFAULT 0,
  `mb_item_ID` mediumint(9) NOT NULL DEFAULT 0,
  `mb_count` decimal(8,0) DEFAULT NULL,
  PRIMARY KEY (`mb_ID`),
  UNIQUE KEY `mb_ID` (`mb_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Copiando dados para a tabela churchcrm_2.multibuy_mb: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `multibuy_mb` DISABLE KEYS */;
/*!40000 ALTER TABLE `multibuy_mb` ENABLE KEYS */;

-- Copiando estrutura para tabela churchcrm_2.note_nte
CREATE TABLE IF NOT EXISTS `note_nte` (
  `nte_ID` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `nte_per_ID` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `nte_fam_ID` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `nte_Private` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `nte_Text` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `nte_DateEntered` datetime NOT NULL,
  `nte_DateLastEdited` datetime DEFAULT NULL,
  `nte_EnteredBy` mediumint(8) NOT NULL DEFAULT 0,
  `nte_EditedBy` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `nte_Type` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nte_Title` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`nte_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Copiando dados para a tabela churchcrm_2.note_nte: ~19 rows (aproximadamente)
/*!40000 ALTER TABLE `note_nte` DISABLE KEYS */;
INSERT IGNORE INTO `note_nte` (`nte_ID`, `nte_per_ID`, `nte_fam_ID`, `nte_Private`, `nte_Text`, `nte_DateEntered`, `nte_DateLastEdited`, `nte_EnteredBy`, `nte_EditedBy`, `nte_Type`, `nte_Title`) VALUES
	(1, 1, 0, 0, 'senha do usuário do sistema mudada', '2019-09-02 18:42:13', NULL, 1, 0, 'user', NULL),
	(2, 2, 0, 0, 'Criada', '2019-09-02 19:28:34', NULL, 1, 0, 'create', NULL),
	(3, 2, 0, 0, 'Atualizado', '2019-09-02 19:30:02', NULL, 1, 0, 'edit', NULL),
	(4, 2, 0, 0, 'Imagem do Perfil Enviada', '2019-09-02 19:33:04', NULL, 1, 0, 'photo', NULL),
	(6, 0, 1, 0, 'Criada', '2019-09-02 20:08:05', NULL, 1, 0, 'create', NULL),
	(8, 2, 0, 0, 'Atualizado', '2019-09-02 20:14:52', NULL, 1, 0, 'edit', NULL),
	(10, 2, 0, 0, 'usuário do sistema criado', '2019-09-07 10:30:24', NULL, 1, 0, 'user', NULL),
	(11, 2, 0, 0, 'Atualizado', '2019-09-07 10:57:55', NULL, 1, 0, 'edit', NULL),
	(13, 2, 0, 0, 'Atualizado', '2019-09-16 20:08:59', NULL, 1, 0, 'edit', NULL),
	(14, 1, 0, 0, 'usuário do sistema atualizado', '2019-09-16 20:15:52', NULL, 1, 0, 'user', NULL),
	(15, 2, 0, 0, 'Atualizado', '2019-09-17 18:56:34', NULL, 1, 0, 'edit', NULL),
	(16, 2, 0, 0, 'Atualizado', '2019-09-17 18:57:17', NULL, 1, 0, 'edit', NULL),
	(17, 2, 0, 0, 'Atualizado', '2019-09-17 19:07:11', NULL, 1, 0, 'edit', NULL),
	(18, 2, 0, 0, 'Atualizado', '2019-09-17 19:08:01', NULL, 1, 0, 'edit', NULL),
	(19, 2, 0, 0, 'Atualizado', '2019-09-17 19:09:29', NULL, 1, 0, 'edit', NULL),
	(20, 2, 0, 0, 'Atualizado', '2019-09-17 19:09:42', NULL, 1, 0, 'edit', NULL),
	(21, 2, 0, 0, '<p><u>Lorem&nbsp;ipsum&nbsp;dolor&nbsp;sit&nbsp;amet&nbsp;consectetur&nbsp;</u>adipisicing&nbsp;elit.&nbsp;Nihil&nbsp;non&nbsp;repellendus&nbsp;mollitia&nbsp;ratione&nbsp;nisi&nbsp;dolorem&nbsp;molestiae&nbsp;adipisci&nbsp;cumque&nbsp;nobis&nbsp;aut&nbsp;quo,&nbsp;temporibus&nbsp;officia&nbsp;expedita&nbsp;laboriosam,&nbsp;omnis&nbsp;quasi&nbsp;cum&nbsp;numquam&nbsp;eveniet.</p>', '2019-09-17 19:25:56', '2019-09-17 23:26:56', 1, 1, 'note', 'doloremque necessitatibus'),
	(22, 2, 0, 0, '<p>ftftfttftftft</p><p>&nbsp;</p><p>plplplplplp</p>', '2019-09-17 19:38:38', '2019-09-17 21:16:28', 1, 1, 'note', 'asdasdasd'),
	(23, 2, 0, 0, '<p>Lorem&nbsp;ipsum&nbsp;dolor&nbsp;sit&nbsp;amet&nbsp;consectetur&nbsp;adipisicing&nbsp;elit.&nbsp;Dolor&nbsp;deserunt&nbsp;est&nbsp;doloremque&nbsp;necessitatibus&nbsp;laboriosam&nbsp;aperiam&nbsp;commodi,&nbsp;tempora&nbsp;id&nbsp;inventore,&nbsp;vitae&nbsp;a,&nbsp;animi&nbsp;eos&nbsp;beatae&nbsp;voluptatem?&nbsp;Sapiente&nbsp;facere&nbsp;quo&nbsp;dolor&nbsp;quasi.</p>', '2019-09-17 20:43:22', '2019-09-17 21:16:47', 1, 1, 'note', 'Dolor deserunt est doloremque necessitatibus');
/*!40000 ALTER TABLE `note_nte` ENABLE KEYS */;

-- Copiando estrutura para tabela churchcrm_2.paddlenum_pn
CREATE TABLE IF NOT EXISTS `paddlenum_pn` (
  `pn_ID` mediumint(9) unsigned NOT NULL AUTO_INCREMENT,
  `pn_fr_ID` mediumint(9) unsigned DEFAULT NULL,
  `pn_Num` mediumint(9) unsigned DEFAULT NULL,
  `pn_per_ID` mediumint(9) NOT NULL DEFAULT 0,
  PRIMARY KEY (`pn_ID`),
  UNIQUE KEY `pn_ID` (`pn_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Copiando dados para a tabela churchcrm_2.paddlenum_pn: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `paddlenum_pn` DISABLE KEYS */;
/*!40000 ALTER TABLE `paddlenum_pn` ENABLE KEYS */;

-- Copiando estrutura para tabela churchcrm_2.person2group2role_p2g2r
CREATE TABLE IF NOT EXISTS `person2group2role_p2g2r` (
  `p2g2r_per_ID` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `p2g2r_grp_ID` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `p2g2r_rle_ID` mediumint(8) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`p2g2r_per_ID`,`p2g2r_grp_ID`),
  KEY `p2g2r_per_ID` (`p2g2r_per_ID`,`p2g2r_grp_ID`,`p2g2r_rle_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Copiando dados para a tabela churchcrm_2.person2group2role_p2g2r: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `person2group2role_p2g2r` DISABLE KEYS */;
/*!40000 ALTER TABLE `person2group2role_p2g2r` ENABLE KEYS */;

-- Copiando estrutura para tabela churchcrm_2.person2volunteeropp_p2vo
CREATE TABLE IF NOT EXISTS `person2volunteeropp_p2vo` (
  `p2vo_ID` mediumint(9) NOT NULL AUTO_INCREMENT,
  `p2vo_per_ID` mediumint(9) DEFAULT NULL,
  `p2vo_vol_ID` mediumint(9) DEFAULT NULL,
  PRIMARY KEY (`p2vo_ID`),
  UNIQUE KEY `p2vo_ID` (`p2vo_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Copiando dados para a tabela churchcrm_2.person2volunteeropp_p2vo: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `person2volunteeropp_p2vo` DISABLE KEYS */;
/*!40000 ALTER TABLE `person2volunteeropp_p2vo` ENABLE KEYS */;

-- Copiando estrutura para tabela churchcrm_2.person_business
CREATE TABLE IF NOT EXISTS `person_business` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT '',
  `description` varchar(250) DEFAULT '',
  `phone` varchar(250) DEFAULT '',
  `logo` varchar(250) DEFAULT '',
  `site` varchar(250) DEFAULT '',
  `person_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela churchcrm_2.person_business: 1 rows
/*!40000 ALTER TABLE `person_business` DISABLE KEYS */;
INSERT IGNORE INTO `person_business` (`id`, `name`, `description`, `phone`, `logo`, `site`, `person_id`, `category_id`) VALUES
	(1, 'FRANCISCO MARQUES DE ARAUJO', 'Formácia popular', '8999212842', NULL, 'www.mybusiness.me', 2, 2);
/*!40000 ALTER TABLE `person_business` ENABLE KEYS */;

-- Copiando estrutura para tabela churchcrm_2.person_custom
CREATE TABLE IF NOT EXISTS `person_custom` (
  `per_ID` mediumint(9) NOT NULL DEFAULT 0,
  `c1` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`per_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Copiando dados para a tabela churchcrm_2.person_custom: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `person_custom` DISABLE KEYS */;
INSERT IGNORE INTO `person_custom` (`per_ID`, `c1`) VALUES
	(2, NULL);
/*!40000 ALTER TABLE `person_custom` ENABLE KEYS */;

-- Copiando estrutura para tabela churchcrm_2.person_custom_master
CREATE TABLE IF NOT EXISTS `person_custom_master` (
  `custom_Order` smallint(6) NOT NULL DEFAULT 0,
  `custom_Field` varchar(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `custom_Name` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `custom_Special` mediumint(8) unsigned DEFAULT NULL,
  `custom_Side` enum('left','right') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'left',
  `custom_FieldSec` tinyint(4) NOT NULL,
  `type_ID` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`custom_Field`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Copiando dados para a tabela churchcrm_2.person_custom_master: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `person_custom_master` DISABLE KEYS */;
INSERT IGNORE INTO `person_custom_master` (`custom_Order`, `custom_Field`, `custom_Name`, `custom_Special`, `custom_Side`, `custom_FieldSec`, `type_ID`) VALUES
	(1, 'c1', 'Status', 13, 'left', 1, 12);
/*!40000 ALTER TABLE `person_custom_master` ENABLE KEYS */;

-- Copiando estrutura para tabela churchcrm_2.person_per
CREATE TABLE IF NOT EXISTS `person_per` (
  `per_ID` mediumint(9) unsigned NOT NULL AUTO_INCREMENT,
  `per_Title` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `per_FirstName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `per_MiddleName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `per_LastName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `per_Suffix` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `per_Address1` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `per_Address2` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `per_City` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `per_State` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `per_Zip` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `per_Country` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `per_HomePhone` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `per_WorkPhone` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `per_CellPhone` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `per_Email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `per_WorkEmail` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `per_BirthMonth` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `per_BirthDay` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `per_BirthYear` year(4) DEFAULT NULL,
  `per_MembershipDate` date DEFAULT NULL,
  `per_Gender` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `per_fmr_ID` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `per_cls_ID` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `per_fam_ID` smallint(5) unsigned NOT NULL DEFAULT 0,
  `per_Envelope` smallint(5) unsigned DEFAULT NULL,
  `per_DateLastEdited` datetime DEFAULT NULL,
  `per_DateEntered` datetime NOT NULL,
  `per_EnteredBy` smallint(5) NOT NULL DEFAULT 0,
  `per_EditedBy` smallint(5) unsigned DEFAULT 0,
  `per_FriendDate` date DEFAULT NULL,
  `per_Flags` mediumint(9) NOT NULL DEFAULT 0,
  `per_FacebookID` bigint(20) unsigned DEFAULT NULL,
  `per_Twitter` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `per_LinkedIn` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `perStatus` int(11) DEFAULT NULL,
  PRIMARY KEY (`per_ID`),
  KEY `per_ID` (`per_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Copiando dados para a tabela churchcrm_2.person_per: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `person_per` DISABLE KEYS */;
INSERT IGNORE INTO `person_per` (`per_ID`, `per_Title`, `per_FirstName`, `per_MiddleName`, `per_LastName`, `per_Suffix`, `per_Address1`, `per_Address2`, `per_City`, `per_State`, `per_Zip`, `per_Country`, `per_HomePhone`, `per_WorkPhone`, `per_CellPhone`, `per_Email`, `per_WorkEmail`, `per_BirthMonth`, `per_BirthDay`, `per_BirthYear`, `per_MembershipDate`, `per_Gender`, `per_fmr_ID`, `per_cls_ID`, `per_fam_ID`, `per_Envelope`, `per_DateLastEdited`, `per_DateEntered`, `per_EnteredBy`, `per_EditedBy`, `per_FriendDate`, `per_Flags`, `per_FacebookID`, `per_Twitter`, `per_LinkedIn`, `perStatus`) VALUES
	(1, NULL, 'Church', NULL, 'Admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, '0000', NULL, 0, 0, 0, 0, NULL, NULL, '2004-08-25 18:00:00', 1, 0, NULL, 0, NULL, NULL, NULL, 1),
	(2, 'Sr', 'Francisco', 'Alisson', 'Silva de Araujo', '', '', '', '', '', '', '', '(869) 981-7647', '', '', '', '', 12, 25, '1979', '1997-06-24', 1, 1, 1, 1, 0, '2019-09-17 19:09:42', '2019-09-02 19:28:34', 1, 1, NULL, 1, 0, '@alissonally', '', 4);
/*!40000 ALTER TABLE `person_per` ENABLE KEYS */;

-- Copiando estrutura para tabela churchcrm_2.person_status
CREATE TABLE IF NOT EXISTS `person_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela churchcrm_2.person_status: 3 rows
/*!40000 ALTER TABLE `person_status` DISABLE KEYS */;
INSERT IGNORE INTO `person_status` (`id`, `description`) VALUES
	(1, 'Active'),
	(6, 'Outro'),
	(4, 'Inactive');
/*!40000 ALTER TABLE `person_status` ENABLE KEYS */;

-- Copiando estrutura para tabela churchcrm_2.pledge_plg
CREATE TABLE IF NOT EXISTS `pledge_plg` (
  `plg_plgID` mediumint(9) NOT NULL AUTO_INCREMENT,
  `plg_FamID` mediumint(9) DEFAULT NULL,
  `plg_FYID` mediumint(9) DEFAULT NULL,
  `plg_date` date DEFAULT NULL,
  `plg_amount` decimal(8,2) DEFAULT NULL,
  `plg_schedule` enum('Weekly','Monthly','Quarterly','Once','Other') COLLATE utf8_unicode_ci DEFAULT NULL,
  `plg_method` enum('CREDITCARD','CHECK','CASH','BANKDRAFT','EGIVE') COLLATE utf8_unicode_ci DEFAULT NULL,
  `plg_comment` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `plg_DateLastEdited` date NOT NULL DEFAULT '2016-01-01',
  `plg_EditedBy` mediumint(9) NOT NULL DEFAULT 0,
  `plg_PledgeOrPayment` enum('Pledge','Payment') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Pledge',
  `plg_fundID` tinyint(3) unsigned DEFAULT NULL,
  `plg_depID` mediumint(9) unsigned DEFAULT NULL,
  `plg_CheckNo` bigint(16) unsigned DEFAULT NULL,
  `plg_Problem` tinyint(1) DEFAULT NULL,
  `plg_scanString` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `plg_aut_ID` mediumint(9) NOT NULL DEFAULT 0,
  `plg_aut_Cleared` tinyint(1) NOT NULL DEFAULT 0,
  `plg_aut_ResultID` mediumint(9) NOT NULL DEFAULT 0,
  `plg_NonDeductible` decimal(8,2) NOT NULL,
  `plg_GroupKey` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`plg_plgID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Copiando dados para a tabela churchcrm_2.pledge_plg: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `pledge_plg` DISABLE KEYS */;
/*!40000 ALTER TABLE `pledge_plg` ENABLE KEYS */;

-- Copiando estrutura para tabela churchcrm_2.propertytype_prt
CREATE TABLE IF NOT EXISTS `propertytype_prt` (
  `prt_ID` mediumint(9) NOT NULL AUTO_INCREMENT,
  `prt_Class` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `prt_Name` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `prt_Description` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`prt_ID`),
  UNIQUE KEY `prt_ID` (`prt_ID`),
  KEY `prt_ID_2` (`prt_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Copiando dados para a tabela churchcrm_2.propertytype_prt: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `propertytype_prt` DISABLE KEYS */;
INSERT IGNORE INTO `propertytype_prt` (`prt_ID`, `prt_Class`, `prt_Name`, `prt_Description`) VALUES
	(1, 'p', 'General', 'General Person Properties'),
	(2, 'f', 'General', 'General Family Properties'),
	(3, 'g', 'General', 'General Group Properties');
/*!40000 ALTER TABLE `propertytype_prt` ENABLE KEYS */;

-- Copiando estrutura para tabela churchcrm_2.property_pro
CREATE TABLE IF NOT EXISTS `property_pro` (
  `pro_ID` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `pro_Class` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `pro_prt_ID` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `pro_Name` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `pro_Description` text COLLATE utf8_unicode_ci NOT NULL,
  `pro_Prompt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`pro_ID`),
  UNIQUE KEY `pro_ID` (`pro_ID`),
  KEY `pro_ID_2` (`pro_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Copiando dados para a tabela churchcrm_2.property_pro: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `property_pro` DISABLE KEYS */;
INSERT IGNORE INTO `property_pro` (`pro_ID`, `pro_Class`, `pro_prt_ID`, `pro_Name`, `pro_Description`, `pro_Prompt`) VALUES
	(1, 'p', 1, 'Disabled', 'has a disability.', 'What is the nature of the disability?'),
	(2, 'f', 2, 'Single Parent', 'is a single-parent household.', ''),
	(3, 'g', 3, 'Youth', 'is youth-oriented.', '');
/*!40000 ALTER TABLE `property_pro` ENABLE KEYS */;

-- Copiando estrutura para tabela churchcrm_2.queryparameteroptions_qpo
CREATE TABLE IF NOT EXISTS `queryparameteroptions_qpo` (
  `qpo_ID` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `qpo_qrp_ID` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `qpo_Display` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `qpo_Value` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`qpo_ID`),
  UNIQUE KEY `qpo_ID` (`qpo_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Copiando dados para a tabela churchcrm_2.queryparameteroptions_qpo: ~36 rows (aproximadamente)
/*!40000 ALTER TABLE `queryparameteroptions_qpo` DISABLE KEYS */;
INSERT IGNORE INTO `queryparameteroptions_qpo` (`qpo_ID`, `qpo_qrp_ID`, `qpo_Display`, `qpo_Value`) VALUES
	(1, 4, 'Male', '1'),
	(2, 4, 'Female', '2'),
	(3, 6, 'Male', '1'),
	(4, 6, 'Female', '2'),
	(5, 15, 'Name', 'CONCAT(COALESCE(`per_FirstName`,\'\'),COALESCE(`per_MiddleName`,\'\'),COALESCE(`per_LastName`,\'\'))'),
	(6, 15, 'Zip Code', 'fam_Zip'),
	(7, 15, 'State', 'fam_State'),
	(8, 15, 'City', 'fam_City'),
	(9, 15, 'Home Phone', 'per_HomePhone'),
	(10, 27, '2012/2013', '17'),
	(11, 27, '2013/2014', '18'),
	(12, 27, '2014/2015', '19'),
	(13, 27, '2015/2016', '20'),
	(14, 28, '2012/2013', '17'),
	(15, 28, '2013/2014', '18'),
	(16, 28, '2014/2015', '19'),
	(17, 28, '2015/2016', '20'),
	(18, 30, '2012/2013', '17'),
	(19, 30, '2013/2014', '18'),
	(20, 30, '2014/2015', '19'),
	(21, 30, '2015/2016', '20'),
	(22, 31, '2012/2013', '17'),
	(23, 31, '2013/2014', '18'),
	(24, 31, '2014/2015', '19'),
	(25, 31, '2015/2016', '20'),
	(26, 15, 'Email', 'per_Email'),
	(27, 15, 'WorkEmail', 'per_WorkEmail'),
	(28, 32, '2012/2013', '17'),
	(29, 32, '2013/2014', '18'),
	(30, 32, '2014/2015', '19'),
	(31, 32, '2015/2016', '20'),
	(32, 33, 'Member', '1'),
	(33, 33, 'Regular Attender', '2'),
	(34, 33, 'Guest', '3'),
	(35, 33, 'Non-Attender', '4'),
	(36, 33, 'Non-Attender (staff)', '5');
/*!40000 ALTER TABLE `queryparameteroptions_qpo` ENABLE KEYS */;

-- Copiando estrutura para tabela churchcrm_2.queryparameters_qrp
CREATE TABLE IF NOT EXISTS `queryparameters_qrp` (
  `qrp_ID` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `qrp_qry_ID` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `qrp_Type` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `qrp_OptionSQL` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `qrp_Name` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `qrp_Description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `qrp_Alias` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `qrp_Default` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `qrp_Required` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `qrp_InputBoxSize` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `qrp_Validation` varchar(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `qrp_NumericMax` int(11) DEFAULT NULL,
  `qrp_NumericMin` int(11) DEFAULT NULL,
  `qrp_AlphaMinLength` int(11) DEFAULT NULL,
  `qrp_AlphaMaxLength` int(11) DEFAULT NULL,
  PRIMARY KEY (`qrp_ID`),
  UNIQUE KEY `qrp_ID` (`qrp_ID`),
  KEY `qrp_ID_2` (`qrp_ID`),
  KEY `qrp_qry_ID` (`qrp_qry_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=203 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Copiando dados para a tabela churchcrm_2.queryparameters_qrp: ~30 rows (aproximadamente)
/*!40000 ALTER TABLE `queryparameters_qrp` DISABLE KEYS */;
INSERT IGNORE INTO `queryparameters_qrp` (`qrp_ID`, `qrp_qry_ID`, `qrp_Type`, `qrp_OptionSQL`, `qrp_Name`, `qrp_Description`, `qrp_Alias`, `qrp_Default`, `qrp_Required`, `qrp_InputBoxSize`, `qrp_Validation`, `qrp_NumericMax`, `qrp_NumericMin`, `qrp_AlphaMinLength`, `qrp_AlphaMaxLength`) VALUES
	(1, 4, 0, NULL, 'Minimum Age', 'The minimum age for which you want records returned.', 'min', '0', 0, 5, 'n', 120, 0, NULL, NULL),
	(2, 4, 0, NULL, 'Maximum Age', 'The maximum age for which you want records returned.', 'max', '120', 1, 5, 'n', 120, 0, NULL, NULL),
	(4, 6, 1, '', 'Gender', 'The desired gender to search the database for.', 'gender', '1', 1, 0, '', 0, 0, 0, 0),
	(5, 7, 2, 'SELECT lst_OptionID as Value, lst_OptionName as Display FROM list_lst WHERE lst_ID=2 ORDER BY lst_OptionSequence', 'Family Role', 'Select the desired family role.', 'role', '1', 0, 0, '', 0, 0, 0, 0),
	(6, 7, 1, '', 'Gender', 'The gender for which you would like records returned.', 'gender', '1', 1, 0, '', 0, 0, 0, 0),
	(8, 9, 2, 'SELECT pro_ID AS Value, pro_Name as Display \r\nFROM property_pro\r\nWHERE pro_Class= \'p\' \r\nORDER BY pro_Name ', 'Property', 'The property for which you would like person records returned.', 'PropertyID', '0', 1, 0, '', 0, 0, 0, 0),
	(9, 10, 2, 'SELECT distinct don_date as Value, don_date as Display FROM donations_don ORDER BY don_date ASC', 'Beginning Date', 'Please select the beginning date to calculate total contributions for each member (i.e. YYYY-MM-DD). NOTE: You can only choose dates that conatain donations.', 'startdate', '1', 1, 0, '0', 0, 0, 0, 0),
	(10, 10, 2, 'SELECT distinct don_date as Value, don_date as Display FROM donations_don\r\nORDER BY don_date DESC', 'Ending Date', 'Please enter the last date to calculate total contributions for each member (i.e. YYYY-MM-DD).', 'enddate', '1', 1, 0, '', 0, 0, 0, 0),
	(14, 15, 0, '', 'Search', 'Enter any part of the following: Name, City, State, Zip, Home Phone, Email, or Work Email.', 'searchstring', '', 1, 0, '', 0, 0, 0, 0),
	(15, 15, 1, '', 'Field', 'Select field to search for.', 'searchwhat', '1', 1, 0, '', 0, 0, 0, 0),
	(16, 11, 2, 'SELECT distinct don_date as Value, don_date as Display FROM donations_don ORDER BY don_date ASC', 'Beginning Date', 'Please select the beginning date to calculate total contributions for each member (i.e. YYYY-MM-DD). NOTE: You can only choose dates that conatain donations.', 'startdate', '1', 1, 0, '0', 0, 0, 0, 0),
	(17, 11, 2, 'SELECT distinct don_date as Value, don_date as Display FROM donations_don\r\nORDER BY don_date DESC', 'Ending Date', 'Please enter the last date to calculate total contributions for each member (i.e. YYYY-MM-DD).', 'enddate', '1', 1, 0, '', 0, 0, 0, 0),
	(18, 18, 0, '', 'Month', 'The birthday month for which you would like records returned.', 'birthmonth', '1', 1, 0, '', 12, 1, 1, 2),
	(19, 19, 2, 'SELECT grp_ID AS Value, grp_Name AS Display FROM group_grp ORDER BY grp_Type', 'Class', 'The sunday school class for which you would like records returned.', 'group', '1', 1, 0, '', 12, 1, 1, 2),
	(20, 20, 2, 'SELECT grp_ID AS Value, grp_Name AS Display FROM group_grp ORDER BY grp_Type', 'Class', 'The sunday school class for which you would like records returned.', 'group', '1', 1, 0, '', 12, 1, 1, 2),
	(21, 21, 2, 'SELECT grp_ID AS Value, grp_Name AS Display FROM group_grp ORDER BY grp_Type', 'Registered students', 'Group of registered students', 'group', '1', 1, 0, '', 12, 1, 1, 2),
	(22, 22, 0, '', 'Month', 'The membership anniversary month for which you would like records returned.', 'membermonth', '1', 1, 0, '', 12, 1, 1, 2),
	(25, 25, 2, 'SELECT vol_ID AS Value, vol_Name AS Display FROM volunteeropportunity_vol ORDER BY vol_Name', 'Volunteer opportunities', 'Choose a volunteer opportunity', 'volopp', '1', 1, 0, '', 12, 1, 1, 2),
	(26, 26, 0, '', 'Months', 'Number of months since becoming a friend', 'friendmonths', '1', 1, 0, '', 24, 1, 1, 2),
	(27, 28, 1, '', 'First Fiscal Year', 'First fiscal year for comparison', 'fyid1', '9', 1, 0, '', 12, 9, 0, 0),
	(28, 28, 1, '', 'Second Fiscal Year', 'Second fiscal year for comparison', 'fyid2', '9', 1, 0, '', 12, 9, 0, 0),
	(30, 30, 1, '', 'First Fiscal Year', 'Pledged this year', 'fyid1', '9', 1, 0, '', 12, 9, 0, 0),
	(31, 30, 1, '', 'Second Fiscal Year', 'but not this year', 'fyid2', '9', 1, 0, '', 12, 9, 0, 0),
	(32, 32, 1, '', 'Fiscal Year', 'Fiscal Year.', 'fyid', '9', 1, 0, '', 12, 9, 0, 0),
	(33, 18, 1, '', 'Classification', 'Member, Regular Attender, etc.', 'percls', '1', 1, 0, '', 12, 1, 1, 2),
	(100, 100, 2, 'SELECT vol_ID AS Value, vol_Name AS Display FROM volunteeropportunity_vol ORDER BY vol_Name', 'Volunteer opportunities', 'First volunteer opportunity choice', 'volopp1', '1', 1, 0, '', 12, 1, 1, 2),
	(101, 100, 2, 'SELECT vol_ID AS Value, vol_Name AS Display FROM volunteeropportunity_vol ORDER BY vol_Name', 'Volunteer opportunities', 'Second volunteer opportunity choice', 'volopp2', '1', 1, 0, '', 12, 1, 1, 2),
	(200, 200, 2, 'SELECT custom_field as Value, custom_Name as Display FROM person_custom_master', 'Custom field', 'Choose customer person field', 'custom', '1', 0, 0, '', 0, 0, 0, 0),
	(201, 200, 0, '', 'Field value', 'Match custom field to this value', 'value', '1', 0, 0, '', 0, 0, 0, 0),
	(202, 201, 3, 'SELECT event_id as Value, event_title as Display FROM events_event ORDER BY event_start DESC', 'Event', 'Select the desired event', 'event', '', 1, 0, '', 0, 0, 0, 0);
/*!40000 ALTER TABLE `queryparameters_qrp` ENABLE KEYS */;

-- Copiando estrutura para tabela churchcrm_2.query_qry
CREATE TABLE IF NOT EXISTS `query_qry` (
  `qry_ID` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `qry_SQL` text COLLATE utf8_unicode_ci NOT NULL,
  `qry_Name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `qry_Description` text COLLATE utf8_unicode_ci NOT NULL,
  `qry_Count` tinyint(1) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`qry_ID`),
  UNIQUE KEY `qry_ID` (`qry_ID`),
  KEY `qry_ID_2` (`qry_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=202 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Copiando dados para a tabela churchcrm_2.query_qry: ~20 rows (aproximadamente)
/*!40000 ALTER TABLE `query_qry` DISABLE KEYS */;
INSERT IGNORE INTO `query_qry` (`qry_ID`, `qry_SQL`, `qry_Name`, `qry_Description`, `qry_Count`) VALUES
	(3, 'SELECT CONCAT(\'<a href=FamilyView.php?FamilyID=\',fam_ID,\'>\',fam_Name,\'</a>\') AS \'Family Name\', COUNT(*) AS \'No.\'\nFROM person_per\nINNER JOIN family_fam\nON fam_ID = per_fam_ID\nGROUP BY per_fam_ID\nORDER BY \'No.\' DESC', 'Family Member Count', 'Returns each family and the total number of people assigned to them.', 0),
	(4, 'SELECT per_ID as AddToCart,CONCAT(\'<a\r\nhref=PersonView.php?PersonID=\',per_ID,\'>\',per_FirstName,\'\r\n\',per_LastName,\'</a>\') AS Name,\r\nCONCAT(per_BirthMonth,\'/\',per_BirthDay,\'/\',per_BirthYear) AS \'Birth Date\',\r\nDATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(CONCAT(per_BirthYear,\'-\',per_BirthMonth,\'-\',per_BirthDay))),\'%Y\')+0 AS  \'Age\'\r\nFROM person_per\r\nWHERE\r\nDATE_ADD(CONCAT(per_BirthYear,\'-\',per_BirthMonth,\'-\',per_BirthDay),INTERVAL\r\n~min~ YEAR) <= CURDATE()\r\nAND\r\nDATE_ADD(CONCAT(per_BirthYear,\'-\',per_BirthMonth,\'-\',per_BirthDay),INTERVAL\r\n(~max~ + 1) YEAR) >= CURDATE()', 'Person by Age', 'Returns any person records with ages between two given ages.', 1),
	(6, 'SELECT COUNT(per_ID) AS Total FROM person_per WHERE per_Gender = ~gender~', 'Total By Gender', 'Total of records matching a given gender.', 0),
	(7, 'SELECT per_ID as AddToCart, CONCAT(per_FirstName,\' \',per_LastName) AS Name FROM person_per WHERE per_fmr_ID = ~role~ AND per_Gender = ~gender~', 'Person by Role and Gender', 'Selects person records with the family role and gender specified.', 1),
	(9, 'SELECT \r\nper_ID as AddToCart, \r\nCONCAT(per_FirstName,\' \',per_LastName) AS Name, \r\nCONCAT(r2p_Value,\' \') AS Value\r\nFROM person_per,record2property_r2p\r\nWHERE per_ID = r2p_record_ID\r\nAND r2p_pro_ID = ~PropertyID~\r\nORDER BY per_LastName', 'Person by Property', 'Returns person records which are assigned the given property.', 1),
	(15, 'SELECT per_ID as AddToCart, CONCAT(\'<a href=PersonView.php?PersonID=\',per_ID,\'>\',COALESCE(`per_FirstName`,\'\'),\' \',COALESCE(`per_MiddleName`,\'\'),\' \',COALESCE(`per_LastName`,\'\'),\'</a>\') AS Name, fam_City as City, fam_State as State, fam_Zip as ZIP, per_HomePhone as HomePhone, per_Email as Email, per_WorkEmail as WorkEmail FROM person_per RIGHT JOIN family_fam ON family_fam.fam_id = person_per.per_fam_id WHERE ~searchwhat~ LIKE \'%~searchstring~%\'', 'Advanced Search', 'Search by any part of Name, City, State, Zip, Home Phone, Email, or Work Email.', 1),
	(18, 'SELECT per_ID as AddToCart, per_BirthDay as Day, CONCAT(per_FirstName,\' \',per_LastName) AS Name FROM person_per WHERE per_cls_ID=~percls~ AND per_BirthMonth=~birthmonth~ ORDER BY per_BirthDay', 'Birthdays', 'People with birthdays in a particular month', 0),
	(21, 'SELECT per_ID as AddToCart, CONCAT(\'<a href=PersonView.php?PersonID=\',per_ID,\'>\',per_FirstName,\' \',per_LastName,\'</a>\') AS Name FROM person_per LEFT JOIN person2group2role_p2g2r ON per_id = p2g2r_per_ID WHERE p2g2r_grp_ID=~group~ ORDER BY per_LastName', 'Registered students', 'Find Registered students', 1),
	(22, 'SELECT per_ID as AddToCart, DAYOFMONTH(per_MembershipDate) as Day, per_MembershipDate AS DATE, CONCAT(per_FirstName,\' \',per_LastName) AS Name FROM person_per WHERE per_cls_ID=1 AND MONTH(per_MembershipDate)=~membermonth~ ORDER BY per_MembershipDate', 'Membership anniversaries', 'Members who joined in a particular month', 0),
	(23, 'SELECT usr_per_ID as AddToCart, CONCAT(a.per_FirstName,\' \',a.per_LastName) AS Name FROM user_usr LEFT JOIN person_per a ON per_ID=usr_per_ID ORDER BY per_LastName', 'Select database users', 'People who are registered as database users', 0),
	(24, 'SELECT per_ID as AddToCart, CONCAT(\'<a href=PersonView.php?PersonID=\',per_ID,\'>\',per_FirstName,\' \',per_LastName,\'</a>\') AS Name FROM person_per WHERE per_cls_id =1', 'Select all members', 'People who are members', 0),
	(25, 'SELECT per_ID as AddToCart, CONCAT(\'<a href=PersonView.php?PersonID=\',per_ID,\'>\',per_FirstName,\' \',per_LastName,\'</a>\') AS Name FROM person_per LEFT JOIN person2volunteeropp_p2vo ON per_id = p2vo_per_ID WHERE p2vo_vol_ID = ~volopp~ ORDER BY per_LastName', 'Volunteers', 'Find volunteers for a particular opportunity', 1),
	(26, 'SELECT per_ID as AddToCart, CONCAT(per_FirstName,\' \',per_LastName) AS Name FROM person_per WHERE DATE_SUB(NOW(),INTERVAL ~friendmonths~ MONTH)<per_FriendDate ORDER BY per_MembershipDate', 'Recent friends', 'Friends who signed up in previous months', 0),
	(27, 'SELECT per_ID as AddToCart, CONCAT(per_FirstName,\' \',per_LastName) AS Name FROM person_per inner join family_fam on per_fam_ID=fam_ID where per_fmr_ID<>3 AND fam_OkToCanvass="TRUE" ORDER BY fam_Zip', 'Families to Canvass', 'People in families that are ok to canvass.', 0),
	(28, 'SELECT fam_Name, a.plg_amount as PlgFY1, b.plg_amount as PlgFY2 from family_fam left join pledge_plg a on a.plg_famID = fam_ID and a.plg_FYID=~fyid1~ and a.plg_PledgeOrPayment=\'Pledge\' left join pledge_plg b on b.plg_famID = fam_ID and b.plg_FYID=~fyid2~ and b.plg_PledgeOrPayment=\'Pledge\' order by fam_Name', 'Pledge comparison', 'Compare pledges between two fiscal years', 1),
	(30, 'SELECT per_ID as AddToCart, CONCAT(per_FirstName,\' \',per_LastName) AS Name, fam_address1, fam_city, fam_state, fam_zip FROM person_per join family_fam on per_fam_id=fam_id where per_fmr_id<>3 and per_fam_id in (select fam_id from family_fam inner join pledge_plg a on a.plg_famID=fam_ID and a.plg_FYID=~fyid1~ and a.plg_amount>0) and per_fam_id not in (select fam_id from family_fam inner join pledge_plg b on b.plg_famID=fam_ID and b.plg_FYID=~fyid2~ and b.plg_amount>0)', 'Missing pledges', 'Find people who pledged one year but not another', 1),
	(32, 'SELECT fam_Name, fam_Envelope, b.fun_Name as Fund_Name, a.plg_amount as Pledge from family_fam left join pledge_plg a on a.plg_famID = fam_ID and a.plg_FYID=~fyid~ and a.plg_PledgeOrPayment=\'Pledge\' and a.plg_amount>0 join donationfund_fun b on b.fun_ID = a.plg_fundID order by fam_Name, a.plg_fundID', 'Family Pledge by Fiscal Year', 'Pledge summary by family name for each fund for the selected fiscal year', 1),
	(100, 'SELECT a.per_ID as AddToCart, CONCAT(\'<a href=PersonView.php?PersonID=\',a.per_ID,\'>\',a.per_FirstName,\' \',a.per_LastName,\'</a>\') AS Name FROM person_per AS a LEFT JOIN person2volunteeropp_p2vo p2v1 ON (a.per_id = p2v1.p2vo_per_ID AND p2v1.p2vo_vol_ID = ~volopp1~) LEFT JOIN person2volunteeropp_p2vo p2v2 ON (a.per_id = p2v2.p2vo_per_ID AND p2v2.p2vo_vol_ID = ~volopp2~) WHERE p2v1.p2vo_per_ID=p2v2.p2vo_per_ID ORDER BY per_LastName', 'Volunteers', 'Find volunteers for who match two specific opportunity codes', 1),
	(200, 'SELECT a.per_ID as AddToCart, CONCAT(\'<a href=PersonView.php?PersonID=\',a.per_ID,\'>\',a.per_FirstName,\' \',a.per_LastName,\'</a>\') AS Name FROM person_per AS a LEFT JOIN person_custom pc ON a.per_id = pc.per_ID WHERE pc.~custom~=\'~value~\' ORDER BY per_LastName', 'CustomSearch', 'Find people with a custom field value', 1),
	(201, 'SELECT per_ID as AddToCart, CONCAT(\'<a href=PersonView.php?PersonID=\',per_ID,\'>\',per_FirstName,\',per_LastName,\'</a>\') AS Name, per_LastName AS Lastname FROM person_per LEFT OUTER JOIN (SELECT event_attend.attend_id, event_attend.person_id FROM event_attend WHERE event_attend.event_id IN (~event~)) a ON person_per.per_ID = a.person_id WHERE a.attend_id is NULL ORDER BY person_per.per_LastName, person_per.per_FirstName', 'Missing people', 'Find people who didn\'t attend an event', 1);
/*!40000 ALTER TABLE `query_qry` ENABLE KEYS */;

-- Copiando estrutura para tabela churchcrm_2.record2property_r2p
CREATE TABLE IF NOT EXISTS `record2property_r2p` (
  `r2p_pro_ID` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `r2p_record_ID` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `r2p_Value` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Copiando dados para a tabela churchcrm_2.record2property_r2p: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `record2property_r2p` DISABLE KEYS */;
/*!40000 ALTER TABLE `record2property_r2p` ENABLE KEYS */;

-- Copiando estrutura para tabela churchcrm_2.result_res
CREATE TABLE IF NOT EXISTS `result_res` (
  `res_ID` mediumint(9) NOT NULL AUTO_INCREMENT,
  `res_echotype1` text COLLATE utf8_unicode_ci NOT NULL,
  `res_echotype2` text COLLATE utf8_unicode_ci NOT NULL,
  `res_echotype3` text COLLATE utf8_unicode_ci NOT NULL,
  `res_authorization` text COLLATE utf8_unicode_ci NOT NULL,
  `res_order_number` text COLLATE utf8_unicode_ci NOT NULL,
  `res_reference` text COLLATE utf8_unicode_ci NOT NULL,
  `res_status` text COLLATE utf8_unicode_ci NOT NULL,
  `res_avs_result` text COLLATE utf8_unicode_ci NOT NULL,
  `res_security_result` text COLLATE utf8_unicode_ci NOT NULL,
  `res_mac` text COLLATE utf8_unicode_ci NOT NULL,
  `res_decline_code` text COLLATE utf8_unicode_ci NOT NULL,
  `res_tran_date` text COLLATE utf8_unicode_ci NOT NULL,
  `res_merchant_name` text COLLATE utf8_unicode_ci NOT NULL,
  `res_version` text COLLATE utf8_unicode_ci NOT NULL,
  `res_EchoServer` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`res_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Copiando dados para a tabela churchcrm_2.result_res: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `result_res` DISABLE KEYS */;
/*!40000 ALTER TABLE `result_res` ENABLE KEYS */;

-- Copiando estrutura para tabela churchcrm_2.tokens
CREATE TABLE IF NOT EXISTS `tokens` (
  `token` varchar(99) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `reference_id` int(9) NOT NULL,
  `valid_until_date` datetime DEFAULT NULL,
  `remainingUses` int(2) DEFAULT NULL,
  PRIMARY KEY (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Copiando dados para a tabela churchcrm_2.tokens: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `tokens` ENABLE KEYS */;

-- Copiando estrutura para tabela churchcrm_2.userconfig_ucfg
CREATE TABLE IF NOT EXISTS `userconfig_ucfg` (
  `ucfg_per_id` mediumint(9) unsigned NOT NULL,
  `ucfg_id` int(11) NOT NULL DEFAULT 0,
  `ucfg_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ucfg_value` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `ucfg_type` enum('text','number','date','boolean','textarea') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'text',
  `ucfg_tooltip` text COLLATE utf8_unicode_ci NOT NULL,
  `ucfg_permission` enum('FALSE','TRUE') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'FALSE',
  `ucfg_cat` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ucfg_per_id`,`ucfg_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Copiando dados para a tabela churchcrm_2.userconfig_ucfg: ~18 rows (aproximadamente)
/*!40000 ALTER TABLE `userconfig_ucfg` DISABLE KEYS */;
INSERT IGNORE INTO `userconfig_ucfg` (`ucfg_per_id`, `ucfg_id`, `ucfg_name`, `ucfg_value`, `ucfg_type`, `ucfg_tooltip`, `ucfg_permission`, `ucfg_cat`) VALUES
	(0, 0, 'bEmailMailto', '1', 'boolean', 'User permission to send email via mailto: links', 'TRUE', ''),
	(0, 1, 'sMailtoDelimiter', ',', 'text', 'Delimiter to separate emails in mailto: links', 'TRUE', ''),
	(0, 5, 'bCreateDirectory', '0', 'boolean', 'User permission to create directories', 'FALSE', 'SECURITY'),
	(0, 6, 'bExportCSV', '0', 'boolean', 'User permission to export CSV files', 'FALSE', 'SECURITY'),
	(0, 7, 'bUSAddressVerification', '0', 'boolean', 'User permission to use IST Address Verification', 'FALSE', ''),
	(0, 10, 'bAddEvent', '0', 'boolean', 'Allow user to add new event', 'FALSE', 'SECURITY'),
	(1, 0, 'bEmailMailto', '1', 'boolean', 'User permission to send email via mailto: links', 'TRUE', ''),
	(1, 1, 'sMailtoDelimiter', ',', 'text', 'user permission to send email via mailto: links', 'TRUE', ''),
	(1, 5, 'bCreateDirectory', '1', 'boolean', 'User permission to create directories', 'TRUE', ''),
	(1, 6, 'bExportCSV', '1', 'boolean', 'User permission to export CSV files', 'TRUE', ''),
	(1, 7, 'bUSAddressVerification', '1', 'boolean', 'User permission to use IST Address Verification', 'TRUE', ''),
	(1, 10, 'bAddEvent', '', 'boolean', 'Allow user to add new event', 'FALSE', 'SECURITY'),
	(2, 0, 'bEmailMailto', '1', 'boolean', 'User permission to send email via mailto: links', 'TRUE', ''),
	(2, 1, 'sMailtoDelimiter', ',', 'text', 'Delimiter to separate emails in mailto: links', 'TRUE', ''),
	(2, 5, 'bCreateDirectory', '', 'boolean', 'User permission to create directories', 'FALSE', 'SECURITY'),
	(2, 6, 'bExportCSV', '', 'boolean', 'User permission to export CSV files', 'FALSE', 'SECURITY'),
	(2, 7, 'bUSAddressVerification', '', 'boolean', 'User permission to use IST Address Verification', 'FALSE', ''),
	(2, 10, 'bAddEvent', '', 'boolean', 'Allow user to add new event', 'FALSE', 'SECURITY');
/*!40000 ALTER TABLE `userconfig_ucfg` ENABLE KEYS */;

-- Copiando estrutura para tabela churchcrm_2.user_usr
CREATE TABLE IF NOT EXISTS `user_usr` (
  `usr_per_ID` mediumint(9) unsigned NOT NULL DEFAULT 0,
  `usr_Password` varchar(500) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `usr_NeedPasswordChange` tinyint(1) unsigned NOT NULL DEFAULT 1,
  `usr_LastLogin` datetime NOT NULL DEFAULT '2000-01-01 00:00:00',
  `usr_LoginCount` smallint(5) unsigned NOT NULL DEFAULT 0,
  `usr_FailedLogins` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `usr_AddRecords` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `usr_EditRecords` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `usr_DeleteRecords` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `usr_MenuOptions` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `usr_ManageGroups` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `usr_Finance` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `usr_Notes` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `usr_Admin` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `usr_SearchLimit` tinyint(4) DEFAULT 10,
  `usr_Style` varchar(50) COLLATE utf8_unicode_ci DEFAULT 'Style.css',
  `usr_showPledges` tinyint(1) NOT NULL DEFAULT 0,
  `usr_showPayments` tinyint(1) NOT NULL DEFAULT 0,
  `usr_showSince` date NOT NULL DEFAULT '2016-01-01',
  `usr_defaultFY` mediumint(9) NOT NULL DEFAULT 10,
  `usr_currentDeposit` mediumint(9) NOT NULL DEFAULT 0,
  `usr_UserName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `usr_apiKey` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `usr_EditSelf` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `usr_CalStart` date DEFAULT NULL,
  `usr_CalEnd` date DEFAULT NULL,
  `usr_CalNoSchool1` date DEFAULT NULL,
  `usr_CalNoSchool2` date DEFAULT NULL,
  `usr_CalNoSchool3` date DEFAULT NULL,
  `usr_CalNoSchool4` date DEFAULT NULL,
  `usr_CalNoSchool5` date DEFAULT NULL,
  `usr_CalNoSchool6` date DEFAULT NULL,
  `usr_CalNoSchool7` date DEFAULT NULL,
  `usr_CalNoSchool8` date DEFAULT NULL,
  `usr_SearchFamily` tinyint(3) DEFAULT NULL,
  `usr_Canvasser` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`usr_per_ID`),
  UNIQUE KEY `usr_UserName` (`usr_UserName`),
  UNIQUE KEY `usr_apiKey` (`usr_apiKey`),
  KEY `usr_per_ID` (`usr_per_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Copiando dados para a tabela churchcrm_2.user_usr: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `user_usr` DISABLE KEYS */;
INSERT IGNORE INTO `user_usr` (`usr_per_ID`, `usr_Password`, `usr_NeedPasswordChange`, `usr_LastLogin`, `usr_LoginCount`, `usr_FailedLogins`, `usr_AddRecords`, `usr_EditRecords`, `usr_DeleteRecords`, `usr_MenuOptions`, `usr_ManageGroups`, `usr_Finance`, `usr_Notes`, `usr_Admin`, `usr_SearchLimit`, `usr_Style`, `usr_showPledges`, `usr_showPayments`, `usr_showSince`, `usr_defaultFY`, `usr_currentDeposit`, `usr_UserName`, `usr_apiKey`, `usr_EditSelf`, `usr_CalStart`, `usr_CalEnd`, `usr_CalNoSchool1`, `usr_CalNoSchool2`, `usr_CalNoSchool3`, `usr_CalNoSchool4`, `usr_CalNoSchool5`, `usr_CalNoSchool6`, `usr_CalNoSchool7`, `usr_CalNoSchool8`, `usr_SearchFamily`, `usr_Canvasser`) VALUES
	(1, '45c4771dcd1cbd65babf3dd8cd70fed56d428fe708183ba1d146f0ad153773d7', 0, '2019-09-18 18:47:22', 11, 0, 0, 0, 0, 0, 0, 0, 0, 1, 10, 'skin-blue-light', 0, 0, '2016-01-01', 10, 0, 'Admin', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0),
	(2, '9278206aa5944498f3696b590cc7efb88b7772e06bb6b700185a58251df5591d', 1, '2019-09-07 10:30:24', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 10, 'skin-black-light', 0, 0, '2016-01-01', 23, 0, 'FranciscoSilva de Araujo', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0);
/*!40000 ALTER TABLE `user_usr` ENABLE KEYS */;

-- Copiando estrutura para tabela churchcrm_2.version_ver
CREATE TABLE IF NOT EXISTS `version_ver` (
  `ver_ID` mediumint(9) unsigned NOT NULL AUTO_INCREMENT,
  `ver_version` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ver_update_start` datetime DEFAULT NULL,
  `ver_update_end` datetime DEFAULT NULL,
  PRIMARY KEY (`ver_ID`),
  UNIQUE KEY `ver_version` (`ver_version`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Copiando dados para a tabela churchcrm_2.version_ver: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `version_ver` DISABLE KEYS */;
INSERT IGNORE INTO `version_ver` (`ver_ID`, `ver_version`, `ver_update_start`, `ver_update_end`) VALUES
	(1, '3.4.0', '2019-09-02 22:36:28', '2019-09-02 22:36:40');
/*!40000 ALTER TABLE `version_ver` ENABLE KEYS */;

-- Copiando estrutura para tabela churchcrm_2.volunteeropportunity_vol
CREATE TABLE IF NOT EXISTS `volunteeropportunity_vol` (
  `vol_ID` int(3) NOT NULL AUTO_INCREMENT,
  `vol_Order` int(3) NOT NULL DEFAULT 0,
  `vol_Active` enum('true','false') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'true',
  `vol_Name` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vol_Description` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`vol_ID`),
  UNIQUE KEY `vol_ID` (`vol_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Copiando dados para a tabela churchcrm_2.volunteeropportunity_vol: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `volunteeropportunity_vol` DISABLE KEYS */;
/*!40000 ALTER TABLE `volunteeropportunity_vol` ENABLE KEYS */;

-- Copiando estrutura para tabela churchcrm_2.whycame_why
CREATE TABLE IF NOT EXISTS `whycame_why` (
  `why_ID` mediumint(9) NOT NULL AUTO_INCREMENT,
  `why_per_ID` mediumint(9) NOT NULL DEFAULT 0,
  `why_join` text COLLATE utf8_unicode_ci NOT NULL,
  `why_come` text COLLATE utf8_unicode_ci NOT NULL,
  `why_suggest` text COLLATE utf8_unicode_ci NOT NULL,
  `why_hearOfUs` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`why_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Copiando dados para a tabela churchcrm_2.whycame_why: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `whycame_why` DISABLE KEYS */;
/*!40000 ALTER TABLE `whycame_why` ENABLE KEYS */;

-- Copiando estrutura para view churchcrm_2.email_count
-- Removendo tabela temporária e criando a estrutura VIEW final
DROP TABLE IF EXISTS `email_count`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `email_count` AS select `email_list`.`email` AS `email`,count(0) AS `total` from `email_list` group by `email_list`.`email`;

-- Copiando estrutura para view churchcrm_2.email_list
-- Removendo tabela temporária e criando a estrutura VIEW final
DROP TABLE IF EXISTS `email_list`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `email_list` AS select `family_fam`.`fam_Email` AS `email`,'family' AS `type`,`family_fam`.`fam_ID` AS `id` from `family_fam` where `family_fam`.`fam_Email` is not null and `family_fam`.`fam_Email` <> '' union select `person_per`.`per_Email` AS `email`,'person_home' AS `type`,`person_per`.`per_ID` AS `id` from `person_per` where `person_per`.`per_Email` is not null and `person_per`.`per_Email` <> '' union select `person_per`.`per_WorkEmail` AS `email`,'person_work' AS `type`,`person_per`.`per_ID` AS `id` from `person_per` where `person_per`.`per_WorkEmail` is not null and `person_per`.`per_WorkEmail` <> '';

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
